package com.mms.foodatex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.mms.base.BaseAppCompatActivity;
import com.mms.base.ErrorType;
import com.mms.foodatex.activity.HomeProfielDetailsActivity;
import com.mms.foodatex.activity.LoginActivity;
import com.mms.foodatex.activity.UpdateProfileActivity;
import com.mms.foodatex.activity.UserProfileActivity;
import com.mms.foodatex.adapter.UserSwipeListAdapter;
import com.mms.foodatex.util.PrefSetup;
import com.mms.web_services.ApiCall;
import com.mms.web_services.requestBean.SearchUserRequest;
import com.mms.web_services.requestBean.SwipeRequest;
import com.mms.web_services.requestBean.UserListNearByRequest;
import com.mms.web_services.responseBean.SearchUserResponse;
import com.yalantis.library.Koloda;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class MainActivity extends BaseAppCompatActivity {

    @BindView(R.id.userList_ma)
    Koloda userList_ma;

    @OnClick(R.id.img_profile_ma)
    void click_img_profile_ma() {
        startActivity(new Intent(this, UserProfileActivity.class));
    }

    @OnClick(R.id.img_pre_action_ma)
    void click_img_pre_action_ma() {

        userList_ma.reloadAdapterData();
//        userList_ma.reloadPreviousCard();
    }

    @OnClick(R.id.img_like_action_ma)
    void click_img_like_action_ma() {
        if (userList_ma != null)
            userList_ma.onClickRight();
        postUserSwipeApiCall(1);
    }

    @OnClick(R.id.img_reject_action_ma)
    void click_img_reject_action_ma() {
        if (userList_ma != null)
            userList_ma.onClickLeft();
        postUserSwipeApiCall(2);
    }

    @OnClick(R.id.img_super_like_action_ma)
    void click_img_super_like_action_ma() {
        if (userList_ma != null)
            userList_ma.onClickRight();
        postUserSwipeApiCall(3);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        initializeView();
//        postUserSwipeApiCall();
    }

    private void postUserSwipeApiCall(int swipAction) {
        SwipeRequest swipeRequest = new SwipeRequest();

        SwipeRequest.Data data = new SwipeRequest.Data();
        SwipeRequest.Swipes swipes = new SwipeRequest.Swipes();
        List<SwipeRequest.Swipes> swipeData = new ArrayList<>();
        data.setSender_u_id(PrefSetup.getInstance().getUserInfo().getData().getUser_id());
        swipes.setReceiver_u_id(userList.get(userList_ma.getVerticalScrollbarPosition()).getUser_id());
        swipes.setSwipe(swipAction);
        swipeData.add(swipes);
        data.setSwipes(swipeData);
        swipeRequest.setData(data);

        ApiCall.getInstance().postUserSwipeApiCall(this, swipeRequest, true, this::onSwipeResponse);
    }

    private void onSwipeResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            Log.e(ApplicationContext.TAG, response.toString());
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void initializeView() {
        super.initializeView();

//        postUserListNearByApiCall();
        postSearchUserApiCall();
    }

    private void postSearchUserApiCall() {
        SearchUserRequest searchUserRequest = new SearchUserRequest();
        SearchUserRequest.Data data = new SearchUserRequest.Data();
        data.setSearch_word("");
        searchUserRequest.setData(data);
        ApiCall.getInstance().postSearchUserApiCall(this, searchUserRequest, true, this::OnSearchUserResponse);
    }

    private List<SearchUserResponse.Data> userList;

    private void OnSearchUserResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            SearchUserResponse searchUserResponse = (SearchUserResponse) o;
            userList = searchUserResponse.getData();
            setAdapter();
        }
    }

    private UserSwipeListAdapter userSwipeListAdapter;

    private void setAdapter() {
        userSwipeListAdapter = new UserSwipeListAdapter(this, userList, (object, position) -> {
//            Intent intent = new Intent(this, HomeProfielDetailsActivity.class);
//            intent.putExtra("Data", (Serializable) object);
//            startActivity(intent);
        });
        userList_ma.setAdapter(userSwipeListAdapter);
    }


    AccessToken accessToken;

    public void userLogout(View view) {
        accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null)
            LoginManager.getInstance().logOut();
        PrefSetup.getInstance().clearPrefSetup();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    public void userProfile(View view) {
        startActivity(new Intent(this, UpdateProfileActivity.class));
    }


}
