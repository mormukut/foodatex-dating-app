package com.mms.foodatex.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mms.base.BaseAppCompatActivity;
import com.mms.base.DoActionType;
import com.mms.base.ErrorType;
import com.mms.foodatex.ApplicationContext;
import com.mms.foodatex.R;
import com.mms.foodatex.adapter.HomeProfileMultiImageAdapter;
import com.mms.foodatex.util.PrefSetup;
import com.mms.utilities.Utilities;
import com.mms.web_services.ApiCall;
import com.mms.web_services.requestBean.SwipeRequest;
import com.mms.web_services.requestBean.UserInfoRequest;
import com.mms.web_services.responseBean.SearchUserResponse;
import com.mms.web_services.responseBean.UserInfoResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class HomeProfielDetailsActivity extends BaseAppCompatActivity {

    @BindView(R.id.img_profile_user_hpa)
    ImageView img_profile_user_hpa;

    @BindView(R.id.progress_bar_hpa)
    ProgressBar progress_bar_hpa;

    @BindView(R.id.txt_address_hpa)
    TextView txt_address_hpa;

    @BindView(R.id.txt_type_hpa)
    TextView txt_type_hpa;

    @BindView(R.id.txt_info_hpa)
    TextView txt_info_hpa;

    @BindView(R.id.tv_height_hpa)
    TextView tv_height_hpa;

    @BindView(R.id.tv_city_hpa)
    TextView tv_city_hpa;

    @BindView(R.id.tv_contact_hpa)
    TextView tv_contact_hpa;

    @BindView(R.id.tv_location_hpa)
    TextView tv_location_hpa;

    @BindView(R.id.tv_age_hpa)
    TextView tv_age_hpa;

    @BindView(R.id.tv_veg_hpa)
    TextView tv_veg_hpa;

    @BindView(R.id.tv_user_is_hpa)
    TextView tv_user_is_hpa;

    @BindView(R.id.tv_allergie_hpa)
    TextView tv_allergie_hpa;

    @BindView(R.id.tv_about_user_text_hpa)
    TextView tv_about_user_text_hpa;

    @BindView(R.id.tv_about_user_heading_hpa)
    TextView tv_about_user_heading_hpa;

    @BindView(R.id.tv_profession_hpa)
    TextView tv_profession_hpa;

    @BindView(R.id.tv_personality_hpa)
    TextView tv_personality_hpa;

    @BindView(R.id.tv_want_kids_hpa)
    TextView tv_want_kids_hpa;

    @BindView(R.id.tv_want_kids_heading_hpa)
    TextView tv_want_kids_heading_hpa;

    @BindView(R.id.tv_has_kids_hpa)
    TextView tv_has_kids_hpa;

    @BindView(R.id.tv_has_kids_heading_hpa)
    TextView tv_has_kids_heading_hpa;

    @BindView(R.id.tv_marital_status_hpa)
    TextView tv_marital_status_hpa;

    @BindView(R.id.tv_user_smoke_hpa)
    TextView tv_user_smoke_hpa;

    @BindView(R.id.tv_user_smoke_heading_hpa)
    TextView tv_user_smoke_heading_hpa;

    @BindView(R.id.tv_user_intereste_hpa)
    TextView tv_user_intereste_hpa;

    @BindView(R.id.tv_user_intereste_heading_hpa)
    TextView tv_user_intereste_heading_hpa;

    @BindView(R.id.tv_food_preference_hpa)
    TextView tv_food_preference_hpa;

    @BindView(R.id.tv_interest_heading_hpa)
    TextView tv_interest_heading_hpa;

    @BindView(R.id.tv_marital_heading_hpa)
    TextView tv_marital_heading_hpa;

    @BindView(R.id.tv_know_more_heading_hpa)
    TextView tv_know_more_heading_hpa;

    @BindView(R.id.tv_user_profile_hpa)
    TextView tv_user_profile_hpa;

    @OnClick(R.id.img_back_hpa)
    void img_back_hpa_click() {
        finish();
    }

    @BindView(R.id.rcv_profile_list_hpa)
    RecyclerView rcv_profile_list_hpa;

    @BindView(R.id.crd_header_hpiv)
    CardView crd_header_hpiv;

    @OnClick(R.id.img_super_like_action_hpa)void click_img_super_like_action_hpa(){
    postUserSwipeApiCall(3);
    }

    @OnClick(R.id.img_reject_action_hpa)void click_img_reject_action_hpa(){
    postUserSwipeApiCall(2);
    }

    @OnClick(R.id.img_like_action_hpa)void click_img_like_action_hpa(){
    postUserSwipeApiCall(1);
    }

    @OnClick(R.id.img_pre_action_hpa)void click_img_pre_action_hpa(){
    finish();
    }


    SearchUserResponse.Data searchUserResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_profiel_details);
        ButterKnife.bind(this);

        initializeView();
    }

    @Override
    public void initializeView() {
        super.initializeView();

        if (getIntent().getSerializableExtra("Data") != null)
            searchUserResponse = (SearchUserResponse.Data) getIntent().getSerializableExtra("Data");

        ApplicationContext.getInstance().loadImage(searchUserResponse.getProfile_image(), img_profile_user_hpa, progress_bar_hpa, R.drawable.ic_launcher_foreground);
        txt_info_hpa.setText(searchUserResponse.getFullname());
        txt_address_hpa.setText(searchUserResponse.getAddress());
//        txt_type_hpa.setText(searchUserResponse.getEmail() + "_");

        Utilities.getInstance().setRecyclerViewUISpanCount(this, rcv_profile_list_hpa, 3, DoActionType.Vertical);
        postUserInfoApiCall();

    }

    private UserInfoRequest userInfoRequest;

    private void postUserInfoApiCall() {
        if (userInfoRequest == null)
            userInfoRequest = new UserInfoRequest();
        UserInfoRequest.Data data = new UserInfoRequest.Data();
//        data.setUser_id("29");
        data.setUser_id(searchUserResponse.getUser_id());
//        data.setUser_id(PrefSetup.getInstance().getUserInfo().getData().getUser_id());      //data.setUser_id(searchUserResponse.getUser_id());
        userInfoRequest.setData(data);
        ApiCall.getInstance().postUserInfoApiCall(this, userInfoRequest, true, this::OnUserInfoResponse);
    }

    private UserInfoResponse userInfoResponse;

    private void OnUserInfoResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {

        if (isSuccess) {
            userInfoResponse = (UserInfoResponse) o;
            uploadUserInfo();

        }
    }

    private void uploadUserInfo() {

        String[] userName = userInfoResponse.getData().getFullname().split(" ");
        ApplicationContext.getInstance().loadImage(userInfoResponse.getData().getProfile_image(), img_profile_user_hpa, progress_bar_hpa, R.drawable.ic_launcher_foreground);
        txt_info_hpa.setText(userInfoResponse.getData().getFullname().isEmpty() ? "-" : userInfoResponse.getData().getFullname());
        txt_address_hpa.setText(userInfoResponse.getData().getAddress().isEmpty() ? "-" : userInfoResponse.getData().getAddress());
        txt_type_hpa.setText(userInfoResponse.getData().getFood_preference().isEmpty() ? "-" : userInfoResponse.getData().getFood_preference());

        tv_height_hpa.setText(userInfoResponse.getData().getHeight().isEmpty() ? "-" : userInfoResponse.getData().getHeight() + "''");
        tv_city_hpa.setText(userInfoResponse.getData().getCity().isEmpty() ? "-" : userInfoResponse.getData().getCity());
        tv_contact_hpa.setText(userInfoResponse.getData().getPhone().isEmpty() ? "-" : userInfoResponse.getData().getPhone());
        tv_location_hpa.setText(userInfoResponse.getData().getAddress().isEmpty() ? "-" : userInfoResponse.getData().getAddress());
        tv_age_hpa.setText(userInfoResponse.getData().getDob().isEmpty() ? "-" : userInfoResponse.getData().getDob());
        tv_veg_hpa.setText(userInfoResponse.getData().getAre_you_a().isEmpty() ? "-" : userInfoResponse.getData().getAre_you_a());
        tv_user_is_hpa.setText(userName == null ? "User" : userName[0] + " is");
        tv_want_kids_heading_hpa.setText(userName == null ? "User" : userName[0] + " want kids ?");
        tv_about_user_heading_hpa.setText("About " + (userName == null ? "User" : userName[0]));
        tv_has_kids_heading_hpa.setText(userName == null ? "User" : userName[0] + " has kids ?");
        tv_user_smoke_heading_hpa.setText(userName == null ? "User" : userName[0] + " Smoke, Take Drugs Or Drink ?");
        tv_user_intereste_heading_hpa.setText(userName == null ? "User" : userName[0] + " Interested");
        tv_interest_heading_hpa.setText(userName == null ? "User" : userName[0] + " Interested");
        tv_user_profile_hpa.setText(userName == null ? "User" : userName[0] + "'s Profile");
        tv_marital_heading_hpa.setText("Marital Details of " + (userName == null ? "User" : userName[0]));
        tv_know_more_heading_hpa.setText("Know More About  " + (userName == null ? "User" : userName[0]));

        tv_allergie_hpa.setText(userInfoResponse.getData().getAny_allergies().isEmpty() ? "-" : userInfoResponse.getData().getAny_allergies());
        tv_about_user_text_hpa.setText(userInfoResponse.getData().getAbout_yourself().isEmpty() ? "-" : userInfoResponse.getData().getAbout_yourself());
        tv_profession_hpa.setText(userInfoResponse.getData().getWhat_do_you().isEmpty() ? "-" : userInfoResponse.getData().getWhat_do_you());
        tv_personality_hpa.setText(userInfoResponse.getData().getPersonality().isEmpty() ? "-" : userInfoResponse.getData().getPersonality());
        tv_want_kids_hpa.setText(userInfoResponse.getData().getDo_you_want_kids().isEmpty() ? "-" : userInfoResponse.getData().getDo_you_want_kids());
        tv_has_kids_hpa.setText(userInfoResponse.getData().getDo_you_have_kids().isEmpty() ? "-" : userInfoResponse.getData().getDo_you_have_kids());
        tv_marital_status_hpa.setText(userInfoResponse.getData().getDo_you_have_kids().isEmpty() ? "-" : userInfoResponse.getData().getDo_you_have_kids());
        tv_user_smoke_hpa.setText(userInfoResponse.getData().getDo_you_smoke().isEmpty() ? "-" : userInfoResponse.getData().getDo_you_smoke().equals("0") ? "No" : "Yes");
        tv_user_intereste_hpa.setText(userInfoResponse.getData().getInterest_in().isEmpty() ? "-" : userInfoResponse.getData().getInterest_in().equals("0") ? "Male" : "Female");
        tv_food_preference_hpa.setText(userInfoResponse.getData().getFood_preference().isEmpty() ? "-" : userInfoResponse.getData().getFood_preference());

        setAdapter();
    }

    private HomeProfileMultiImageAdapter imageAdapter;

    private void setAdapter() {
        if (userInfoResponse.getData().getImages() == null) {
            crd_header_hpiv.setVisibility(View.GONE);
        } else {
            crd_header_hpiv.setVisibility(View.VISIBLE);
            imageAdapter = new HomeProfileMultiImageAdapter(this, userInfoResponse.getData().getImages());
            rcv_profile_list_hpa.setAdapter(imageAdapter);
        }
    }

    private void postUserSwipeApiCall(int swipAction) {
        SwipeRequest swipeRequest = new SwipeRequest();

        SwipeRequest.Data data = new SwipeRequest.Data();
        SwipeRequest.Swipes swipes = new SwipeRequest.Swipes();
        List<SwipeRequest.Swipes> swipeData = new ArrayList<>();
        data.setSender_u_id(PrefSetup.getInstance().getUserInfo().getData().getUser_id());
        swipes.setReceiver_u_id(userInfoResponse.getData().getUser_id());
        swipes.setSwipe(swipAction);
        swipeData.add(swipes);
        data.setSwipes(swipeData);
        swipeRequest.setData(data);

        ApiCall.getInstance().postUserSwipeApiCall(this, swipeRequest, true, this::onSwipeResponse);
    }

    private void onSwipeResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            Log.e(ApplicationContext.TAG, response.toString());
        }
    }


}
