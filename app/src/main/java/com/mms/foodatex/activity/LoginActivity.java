package com.mms.foodatex.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.ImageViewCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.mms.base.BaseAppCompatActivity;
import com.mms.base.ErrorType;
import com.mms.foodatex.ApplicationContext;
import com.mms.foodatex.MainActivity;
import com.mms.foodatex.R;
import com.mms.foodatex.util.PrefSetup;
import com.mms.utilities.Utilities;
import com.mms.web_services.ApiCall;
import com.mms.web_services.requestBean.SocialLoginRequest;
import com.mms.web_services.requestBean.UserInfoModel;

import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class LoginActivity extends BaseAppCompatActivity {
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.layoutDots)
    LinearLayout dotsLayout;

    @OnClick(R.id.txt_phone_login)
    void click_txt_phone_login() {
        startPhoneActivity();
    }

    private void startPhoneActivity() {
        startActivity(new Intent(this, LoginByPhoneActivity.class));
    }

    private int[] layouts;
    private ImageView[] dots;
    private MyViewPagerAdapter myViewPagerAdapter;

    @OnClick(R.id.txt_facebook_action_la)
    void click_txt_facebook_action_la() {
        fb_Btn_Click();
    }

    //        .........................facebook.........................
    AccessToken accessToken;
    private CallbackManager mCallbackManager;

    private static final String NAME = "name";
    private static final String ID = "id";
    private static final String EMAIL = "email";
    private static final String PICTURE = "picture.type(large)";
    private static final String BIRTHDAY = "birthday";
    private static final String GENDER = "gender";
    private static final String LINK = "link";
    private static final String LOCATION = "location";
    private static final String LOCALE = "locale";
    private static final String TIMEZONE = "timezone";
    private static final String UPDATED_TIME = "updated_time";
    private static final String VERIFIED = "verified";


    public static final String FIELDS = "fields";
    public static final String REQUEST_FIELDS =
            TextUtils.join(",", new String[]{ID, NAME, PICTURE, BIRTHDAY, GENDER, LINK, LOCATION, LOCALE, TIMEZONE, UPDATED_TIME, VERIFIED, EMAIL});
    private static final String TAG_FEED = "feed", TAG_ENTRY = "entry", TAG_GPHOTO_ID = "gphoto$id", TAG_T = "$t",
            TAG_ALBUM_TITLE = "title";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        FBLogout();
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        loginFacebook();

//        showHashKey();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (PrefSetup.getInstance().getUserInfo() != null) {
            if (PrefSetup.getInstance().getUserInfo().getProfile_staus().equals("1")) {
                startMainActivity();
            } else {
                startUpdateProfileActivity();
            }
        }

        initializeView();
    }


    @Override
    protected void onResume() {
        super.onResume();
        getInternet();
    }

    @Override
    public void initializeView() {
        super.initializeView();
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide3,
        };

        // adding bottom dots
        addBottomDots(0);

        // making notification bar transparent
        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };


    private void addBottomDots(int currentPage) {
        dots = new ImageView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageResource(R.drawable.ic_remove_black_24dp);
            ImageViewCompat.setImageTintList(dots[i], ColorStateList.valueOf(colorsInactive[currentPage]));
//            dots[i].setColorFilter(ContextCompat.getColor(this, colorsInactive[currentPage]), android.graphics.PorterDuff.Mode.SRC_IN);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            ImageViewCompat.setImageTintList(dots[currentPage], ColorStateList.valueOf(colorsActive[currentPage]));
//            dots[currentPage].setColorFilter(ContextCompat.getColor(this, colorsActive[currentPage]), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    private void getInternet() {
        Utilities.getInstance().isOnlineWithRtyPopup(this, isSuccess -> {
            Log.e(ApplicationContext.TAG, isSuccess.toString());
            getInternet();
        });
    }

    /**
     * Facebook login
     */

    public void fb_Btn_Click() {
        if (!Utilities.getInstance().isOnline(this)) {
            makeToast(getString(R.string.noInternetAccess));
            return;
        }

        showProgressBar();
        LoginManager.getInstance().logInWithReadPermissions(
                LoginActivity.this,
                Arrays.asList("public_profile",
                        "email",
                        "user_friends",
                        "user_birthday"));
    }


    public void FBLogout() {

        if (accessToken != null) {
            LoginManager.getInstance().logOut();
        }
    }


    UserInfoModel data;

    private void fetchUserInfo() {
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken, new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {

                            Log.e("response:", response + " ");
                            try {
                                Log.e(ApplicationContext.TAG, object.toString());
                                data = new Gson().fromJson(object.toString(), UserInfoModel.class);
                                postSocialLoginApiCall();
//                                makeToast(object.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString(FIELDS, REQUEST_FIELDS);
            request.setParameters(parameters);
            GraphRequest.executeBatchAsync(request);
        }
    }

    private SocialLoginRequest socialLoginRequest;
    private SocialLoginRequest.Data socialLoginRequestData;

    private void postSocialLoginApiCall() {
        if (socialLoginRequest == null)
            socialLoginRequest = new SocialLoginRequest();
        if (socialLoginRequestData == null)
            socialLoginRequestData = new SocialLoginRequest.Data();
        socialLoginRequestData.setSocial_id(data.getId());
        socialLoginRequestData.setLogintype("facebook");
        socialLoginRequestData.setFullname(data.getName());
        socialLoginRequestData.setEmail(data.getEmail());
        socialLoginRequest.setData(socialLoginRequestData);
        ApiCall.getInstance().postSocialLoginApiCall(this, socialLoginRequest, true, this::OnSocialLoginResponse);
    }

    private void OnSocialLoginResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            if (PrefSetup.getInstance().getUserInfo().getProfile_staus().equals("1")) {
                startMainActivity();
            } else {
                startUpdateProfileActivity();
            }
        } else {
            FBLogout();
        }
    }

    private void startUpdateProfileActivity() {
        startActivity(new Intent(this, UpdateProfileActivity.class));
        finish();
    }

    private void startMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void loginFacebook() {
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("Success", "Login");
                        fetchUserInfo();
                    }

                    @Override
                    public void onCancel() {
                        hideProgressBar();
                        makeToast("Login Cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        hideProgressBar();
                        Log.e(ApplicationContext.TAG, exception.getMessage());
                        makeToast(getString(R.string.noInternetAccess));
                    }
                });
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }


}
