package com.mms.foodatex.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mms.base.BaseAppCompatActivity;
import com.mms.foodatex.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginByPhoneActivity extends BaseAppCompatActivity {

    @OnClick(R.id.crd_back_action)
    void click_crd_back_action() {
    onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_by_phone);
        ButterKnife.bind(this);
    }
}
