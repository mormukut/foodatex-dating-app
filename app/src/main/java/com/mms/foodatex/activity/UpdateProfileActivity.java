package com.mms.foodatex.activity;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.hbb20.CountryCodePicker;
import com.mms.base.BaseAppCompatActivity;
import com.mms.base.CameraListener;
import com.mms.base.DoActionType;
import com.mms.base.ErrorType;
import com.mms.base.OnTaskCompletedListener;
import com.mms.base.PlaceDetailsJSONParser;
import com.mms.base.RealPathUtil;
import com.mms.foodatex.ApplicationContext;
import com.mms.foodatex.MainActivity;
import com.mms.foodatex.R;
import com.mms.foodatex.adapter.ImageListAdapter;
import com.mms.foodatex.adapter.LatLong.PlacesAutoCompleteAdapter;
import com.mms.foodatex.adapter.PickerAdapter;
import com.mms.foodatex.bean.FoodPreferenceBean;
import com.mms.foodatex.bean.SubmitAddBean;
import com.mms.foodatex.fragment.FoodPreference.CityListDialogFragment;
import com.mms.foodatex.fragment.FoodPreference.FoodChoiceDialogFragment;
import com.mms.foodatex.fragment.FoodPreference.FoodPreferenceDialogFragment;
import com.mms.foodatex.fragment.FoodPreference.HeightListDialogFragment;
import com.mms.foodatex.fragment.FoodPreference.MeritalStatusDialogFragment;
import com.mms.foodatex.fragment.FoodPreference.WantKidsListDialogFragment;
import com.mms.foodatex.util.PopupUtils;
import com.mms.foodatex.util.PrefSetup;
import com.mms.utilities.Constant;
import com.mms.utilities.Utilities;
import com.mms.web_services.ApiCall;
import com.mms.web_services.requestBean.UpdateProfileRequest;
import com.mms.web_services.requestBean.UploadImage;
import com.mms.web_services.responseBean.UploadImageResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import travel.ithaka.android.horizontalpickerlib.PickerLayoutManager;

import static com.mms.utilities.Constant.CAPTURE_PHOTO;
import static com.mms.utilities.Constant.SELECT_PHOTO;
import static com.mms.utilities.Constant.SELECT_VIDEO;
import static com.mms.utilities.Constant.VIDEO_CAPTURE;

public class UpdateProfileActivity extends BaseAppCompatActivity {

    private PickerAdapter pickerAdapter;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    @OnClick(R.id.img_camera_action)
    void click_img_camera_action() {
        if (Utilities.getInstance().checkCameraPermission(this)) {
            opencameraPopup();
        }
    }

//    @OnClick(R.id.img_video_action)
//    void click_img_video_action() {
//        openvideoPopup();
//
//    }

    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindViews({R.id.radio_male_upa, R.id.radio_female_upa, R.id.radio_both_upa})
    List<RadioButton> genderRadioButtonList;

    @BindViews({R.id.radio_smoke_yes_upa, R.id.radio_smoke_no_upa})
    List<RadioButton> smokeRadioButtonList;

    @BindViews({R.id.radio_split_yes_upa, R.id.radio_split_no_upa})
    List<RadioButton> spliteRadioButtonList;

    @BindViews({R.id.radio_working_upa, R.id.radio_studying_upa})
    List<RadioButton> professionRadioButtonList;

    @BindViews({R.id.radio_yes_allergies_upa, R.id.radio_no_allergies_upa})
    List<RadioButton> allergiesRadioButtonList;

    @BindViews({R.id.radio_romance_upa, R.id.radio_friendship_upa})
    List<RadioButton> lookingRadioButtonList;

    @OnClick(R.id.btn_next_action_upa)
    void click_btn_next_action_upa() {
        showNextView();
    }

    @BindView(R.id.btn_next_action_upa)
    Button btn_next_action_upa;

    @OnClick(R.id.img_back_upa)
    void click_img_back_upa() {
        showPreView();
    }

    @BindView(R.id.txt_pagenumber_upa)
    TextView txt_pagenumber_upa;

    @BindView(R.id.img_back_upa)
    ImageView img_back_upa;

    @BindView(R.id.rv_age_picker)
    RecyclerView rv_age_picker;

    @BindViews({R.id.ll_first_upa, R.id.ll_second_upa, R.id.ll_third_upa, R.id.ll_fourth_upa,
            R.id.ll_fifth_upa, R.id.ll_sixth_upa, R.id.ll_seventh_upa})
    List<LinearLayout> layoutList;

    @BindView(R.id.txt_loc_upa)
    AutoCompleteTextView txt_loc_upa;

    @BindView(R.id.et_contact_upa)
    EditText et_contact_upa;

    @OnClick(R.id.txt_food_pref_upa)
    void click_txt_food_pref_upa() {
        showFoodPreferenceDialogFragment();
    }

    @BindView(R.id.txt_food_pref_upa)
    TextView txt_food_pref_upa;

    @OnClick(R.id.txt_food_choice_upa)
    void click_txt_food_choice_upa() {
        showChoiceDialogFragment();
    }

    @BindView(R.id.et_allergies_upa)
    EditText et_allergies_upa;

    @BindView(R.id.radiogroup_allergies_upa)
    RadioGroup radiogroup_allergies_upa;

    @BindView(R.id.txt_food_choice_upa)
    TextView txt_food_choice_upa;

    @OnClick(R.id.txt_city_upa)
    void click_txt_city_upa() {
        showCityListDialogFragment();
    }

    @BindView(R.id.txt_city_upa)
    TextView txt_city_upa;

    @BindView(R.id.et_porsonality_upa)
    TextView et_porsonality_upa;

    @OnClick(R.id.txt_height_upa)
    void click_txt_height_upa() {
        showHeightListDialogFragment();
    }

    @BindView(R.id.txt_height_upa)
    TextView txt_height_upa;

    @OnClick(R.id.txt_want_kids_upd)
    void click_txt_want_kids_upd() {
        showWantKidsDialogFragment();
    }

    @BindView(R.id.txt_want_kids_upd)
    TextView txt_want_kids_upd;

    @OnClick(R.id.txt_have_kids_upa)
    void click_txt_have_kids_upa() {
        showHaveKidsDialogFragment();
    }

    @BindView(R.id.txt_have_kids_upa)
    TextView txt_have_kids_upa;

    @OnClick(R.id.txt_merital_status_upa)
    void click_txt_merital_status_upa() {
        showMeritalDialogFragment();
    }

    @BindView(R.id.txt_merital_status_upa)
    TextView txt_merital_status_upa;

    @BindView(R.id.radiogroup_gender_upa)
    RadioGroup radiogroup_gender_upa;

    @BindView(R.id.radiogroup_what_do_you_upa)
    RadioGroup radiogroup_what_do_you_upa;

    @BindView(R.id.radiogroup_smoke_upa)
    RadioGroup radiogroup_smoke_upa;

    @BindView(R.id.radiogroup_splite_bill_upa)
    RadioGroup radiogroup_splite_bill_upa;

    @BindView(R.id.et_description_upa)
    EditText et_description_upa;

    @BindView(R.id.rcv_image_upa)
    RecyclerView rcv_image_upa;

    @BindView(R.id.txt_no_record_image_upa)
    TextView txt_no_record_image_upa;

    /**
     * Provides the entry point to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;


    @OnClick(R.id.img_location_upa)
    void click_img_location_upa() {

        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        initializeView();
    }


    @Override
    protected void onStart() {
        super.onStart();
        Utilities.getInstance().checkCameraPermission(this);
//        Utilities.getInstance().checkRecordingPermission(this);

    }

    private String userAge = "18";

    @Override
    public void initializeView() {
        super.initializeView();
//        Utilities.getInstance().setRecyclerViewUISpanCount(this, rv_age_picker, 1, DoActionType.Vertical);
        Utilities.getInstance().setRecyclerViewUISpanCount(this, rcv_image_upa, 2, DoActionType.Horizontal);
        PickerLayoutManager pickerLayoutManager = new PickerLayoutManager(this, PickerLayoutManager.HORIZONTAL, false);
        pickerLayoutManager.setChangeAlpha(true);
        pickerLayoutManager.setScaleDownBy(0.99f);
        pickerLayoutManager.setScaleDownDistance(0.8f);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(rv_age_picker);
        pickerAdapter = new PickerAdapter(this, setData(100), rv_age_picker);
        rv_age_picker.setAdapter(pickerAdapter);
        rv_age_picker.setLayoutManager(pickerLayoutManager);

        pickerLayoutManager.setOnScrollStopListener(new PickerLayoutManager.onScrollStopListener() {
            @Override
            public void selectedView(View view) {
                //Do your thing
                userAge = ((TextView) view).getText().toString();
//                Toast.makeText(UpdateProfileActivity.this, ("Selected value : " + ((TextView) view).getText().toString()), Toast.LENGTH_SHORT).show();

            }
        });

        radiogroup_allergies_upa.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton radioButton = findViewById(radioGroup.getCheckedRadioButtonId());
                et_allergies_upa.setVisibility(radioButton.getTag().equals("Yes") ? View.VISIBLE : View.GONE);
            }
        });
        setLocationCantrol();

        showFirstView();

        genderRadioButtonList.get(0).setChecked(true);
        lookingRadioButtonList.get(0).setChecked(true);
        allergiesRadioButtonList.get(0).setChecked(true);
        professionRadioButtonList.get(0).setChecked(true);
        smokeRadioButtonList.get(1).setChecked(true);
        spliteRadioButtonList.get(0).setChecked(true);

    }

    private SubmitAddBean submitAddBean;

    private void setLocationCantrol() {
        final PlacesAutoCompleteAdapter place_adapter = new PlacesAutoCompleteAdapter(this, R.layout.autocomplete_list);
        txt_loc_upa.setAdapter(place_adapter);
        txt_loc_upa.setThreshold(1);
        txt_loc_upa.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                try {
                    JSONObject jObj = place_adapter.getJSON(position);
                    if (Utilities.getInstance().isOnline(UpdateProfileActivity.this)) {
                        DownloadTaskRG placeDetailsDownloadTask = new DownloadTaskRG(object -> {
                            Log.e(ApplicationContext.TAG, "onItemClick: " + object, null);
                            submitAddBean = (SubmitAddBean) object;
//                            if (bean == null)
//                                return;
////                            txt_loc.setText(bean.getAddress1());
//                            edit_shippintInfoList.get(3).setText(bean.getSubLocality());
//                            edit_shippintInfoList.get(4).setText(bean.getLocality());
//                            edit_shippintInfoList.get(5).setText(bean.getZip());
                        });
                        String url = Utilities.getInstance().getPlaceDetailsUrl(jObj.getString("place_id"));
                        placeDetailsDownloadTask.execute(url);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private List<String> setData(int count) {
        List<String> data = new ArrayList<>();
        data.add(" ");
        data.add(" ");
        data.add(" ");
        for (int i = 18; i < count; i++) {
            data.add(String.valueOf(i));
        }
        data.add(" ");
        data.add(" ");
        data.add(" ");
        return data;
    }

    AccessToken accessToken;

    public void userLogout() {
        accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null)
            LoginManager.getInstance().logOut();
        PrefSetup.getInstance().clearPrefSetup();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    int viewPosition = -1;

    private void showNextView() {
        Utilities.getInstance().hideKeyboard(this);
        if (viewPosition == -1) {
            showFirstView();
        } else if (viewPosition == 0) {
            if (isFirstViewValid())
                showSecondView();
        } else if (viewPosition == 1) {
            if (isSecondViewValid())
                showThirdView();
        } else if (viewPosition == 2) {
            if (isThirdViewValid())
                showForthView();
        } else if (viewPosition == 3) {
            if (isForthViewValid())
                showFifthView();
        } else if (viewPosition == 4) {
            if (isFifthViewValid())
                showSixthView();
        } else if (viewPosition == 5) {
            if (isSixthViewValid())
                showSeventhView();
        } else if (viewPosition == 6) {
            if (isSeventhViewValide()) {
                if (file != null) {
                    postEditProfileImage();
                } else {
                    postUpdateProfileApiCall();
                }
            }
        }
    }

    private void showPreView() {
        if (viewPosition == 0) {
            finish();
        } else if (viewPosition == 1) {
            showFirstView();
        } else if (viewPosition == 2) {
            showSecondView();
        } else if (viewPosition == 3) {
            showThirdView();
        } else if (viewPosition == 4) {
            showForthView();
        } else if (viewPosition == 5) {
            showFifthView();
        } else if (viewPosition == 6) {
            showSixthView();
        }
    }


    @Override
    public void onBackPressed() {
        showPreView();
    }

    private void showSeventhView() {
        viewPosition = 6;
        visibleLayout();
    }

    private void showSixthView() {
        Utilities.getInstance().checkCameraPermission(this);
        viewPosition = 5;
        visibleLayout();
    }

    private void showFifthView() {
        viewPosition = 4;
        visibleLayout();
    }

    private void showForthView() {
        viewPosition = 3;
        visibleLayout();
    }

    private void showThirdView() {
        viewPosition = 2;
        visibleLayout();
    }

    private void showSecondView() {
        viewPosition = 1;
        visibleLayout();
    }

    private void showFirstView() {
        viewPosition = 0;
        visibleLayout();

    }

    private boolean isSeventhViewValide() {

        RadioButton radioButtonv = findViewById(radiogroup_splite_bill_upa.getCheckedRadioButtonId());
        updateProfileRequestData.setSplit_bill(String.valueOf(radioButtonv.getTag()));
//        updateProfileRequestData.setDescrption(et_description_upa.getText().toString().trim());
        return true;
    }


    private boolean isSixthViewValid() {
        RadioButton radioButtonv = findViewById(radiogroup_smoke_upa.getCheckedRadioButtonId());
        updateProfileRequestData.setDo_you_smoke(String.valueOf(radioButtonv.getTag()));
        return true;
    }

    private boolean isFifthViewValid() {
        updateProfileRequestData.setDo_you_have_kids(txt_have_kids_upa.getText().toString().trim());
        updateProfileRequestData.setMarital_status(txt_merital_status_upa.getText().toString().trim());
        updateProfileRequestData.setDo_you_want_kids(txt_want_kids_upd.getText().toString().trim());
        return true;
    }

    private boolean isForthViewValid() {

//        updateProfileRequestData.setHeight(txt_height_upa.getText().toString().trim());
        updateProfileRequestData.setPersonality(et_porsonality_upa.getText().toString().trim());
        RadioButton radioButtonv = findViewById(radiogroup_what_do_you_upa.getCheckedRadioButtonId());
        updateProfileRequestData.setWhat_do_you(String.valueOf(radioButtonv.getTag()));
        return true;
    }

    private boolean isThirdViewValid() {

        if (txt_city_upa.getText().toString().trim().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.alert),
                    getString(R.string.select_city_name), (object, position) -> {

                    });
            return false;
        }
        updateProfileRequestData.setAre_you_a(txt_food_choice_upa.getText().toString());
        updateProfileRequestData.setCity(txt_city_upa.getText().toString());
        updateProfileRequestData.setAddress(txt_city_upa.getText().toString());
        return true;
    }

    private boolean isSecondViewValid() {
        if (txt_food_pref_upa.getText().toString().trim().isEmpty()) {

            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.alert),
                    getString(R.string.select_food_preference), (object, position) -> {

                    });
            return false;
        }

        updateProfileRequestData.setFood_preference(txt_food_pref_upa.getText().toString());
        RadioButton radioButtonv = findViewById(radiogroup_gender_upa.getCheckedRadioButtonId());
        updateProfileRequestData.setInterest_in(Integer.parseInt(String.valueOf(radioButtonv.getTag())));
//        updateProfileRequestData.setInterest_in((Integer) radioButtonv.getTag());

        return true;
    }

    private boolean isFirstViewValid() {
        if (txt_loc_upa.getText().toString().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.alert),
                    getString(R.string.place_not_found), (object, position) -> {

                    });
            return false;
        } else if (et_contact_upa.getText().toString().isEmpty()) {
            PopupUtils.getInstance().showOkButtonDialogNoCancelable(this, getString(R.string.alert),
                    getString(R.string.please_confirm_phone_country_code_and_enter_your_phone_number), (object, position) -> {

                    });
            return false;
        }

        if (updateProfileRequest == null)
            updateProfileRequest = new UpdateProfileRequest();
        if (updateProfileRequestData == null)
            updateProfileRequestData = new UpdateProfileRequest.Data();
        updateProfileRequestData.setCountry_code(String.valueOf(ccp.getSelectedCountryCodeAsInt()));
        updateProfileRequestData.setUser_id(PrefSetup.getInstance().getUserInfo() == null ? "1"
                : PrefSetup.getInstance().getUserInfo().getData().getUser_id());

        updateProfileRequestData.setLatitude(submitAddBean != null ? submitAddBean.getLatitude() : "26.666621");
        updateProfileRequestData.setLongitude(submitAddBean != null ? submitAddBean.getLongitude() : "56.242423");
        updateProfileRequestData.setDob(userAge);
        updateProfileRequestData.setPhone(et_contact_upa.getText().toString().trim());
        updateProfileRequestData.setAddress(txt_loc_upa.getText().toString().trim());

//        updateProfileRequest.setData(updateProfileRequestData);

        return true;
    }

    private void visibleLayout() {
        for (int x = 0; x < layoutList.size(); x++) {
            layoutList.get(x).setVisibility(viewPosition == x ? View.VISIBLE : View.GONE);
        }
        txt_pagenumber_upa.setText((viewPosition + 1) + "/7");
        img_back_upa.setVisibility(viewPosition == 0 ? View.GONE : View.VISIBLE);
        btn_next_action_upa.setText(viewPosition == 6 ? "Sign up" : "Next");
    }

    /**
     * A method to download json data from url
     */
    public class DownloadTaskRG extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
        String data = null;
        private OnTaskCompletedListener listener;
        private double latitude;
        private double longitude;
        private List<Address> addressList;
        private LatLng temp;
        private SubmitAddBean addressBean = new SubmitAddBean();

        public DownloadTaskRG(OnTaskCompletedListener listener) {
            this.listener = listener;
        }

        @Override
        protected List<HashMap<String, String>> doInBackground(String... url) {
            JSONObject jObj = new JSONObject();
            try {
                data = Utilities.getInstance().downloadUrl(url[0]);
                jObj = new JSONObject(data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
//            List<HashMap<String, String>> obj = PlaceDetailsJSONParser.parse(jObj);
            return PlaceDetailsJSONParser.parse(jObj);
        }


        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            if (result != null) {
                try {
                    temp = new LatLng(Double.parseDouble(result.get(0).get("lat")), Double.parseDouble(result.get(0).get("lng")));
                    latitude = Double.parseDouble(result.get(0).get("lat"));
                    longitude = Double.parseDouble(result.get(0).get("lng"));
                    addressList = Utilities.getInstance().getAddressInList(UpdateProfileActivity.this, latitude, longitude);
                    addressBean.setLatitude(String.valueOf(latitude));
                    addressBean.setLongitude(String.valueOf(longitude));
                    addressBean.setAddress1(txt_loc_upa.getText().toString().trim());
                    addressBean.setAddress2(addressList.get(0).getLocality());
                    addressBean.setSubLocality(addressList.get(0).getSubLocality());
                    addressBean.setLocality(addressList.get(0).getLocality());
                    addressBean.setZip(addressList.get(0).getPostalCode());
                    listener.onTaskCompleted(addressBean);
                    Log.e("LAT LONG - ", temp.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                return;
            }
        }
    }

    private static final int MY_REQUEST_CODE = -1;

    private ArrayList<FoodPreferenceBean> foodPreferenceBeanArrayList;

    public final void showFoodPreferenceDialogFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        FoodPreferenceDialogFragment dialogFragment = FoodPreferenceDialogFragment.Instance((object, position) -> {
            foodPreferenceBeanArrayList = (ArrayList<FoodPreferenceBean>) object;
            setFoodpreferenceName();
        });
        dialogFragment.setTargetFragment(null, MY_REQUEST_CODE);
        dialogFragment.show(ft, "dialog");
    }

    public final void showChoiceDialogFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        FoodChoiceDialogFragment dialogFragment = FoodChoiceDialogFragment.Instance((object, position) -> {
            foodPreferenceBeanArrayList = (ArrayList<FoodPreferenceBean>) object;
            setFoodChoiceName();
        });
        dialogFragment.setTargetFragment(null, MY_REQUEST_CODE);
        dialogFragment.show(ft, "dialog");
    }

    public final void showCityListDialogFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        CityListDialogFragment dialogFragment = CityListDialogFragment.Instance((object, position) -> {
            foodPreferenceBeanArrayList = (ArrayList<FoodPreferenceBean>) object;
            setCityName();
        });
        dialogFragment.setTargetFragment(null, MY_REQUEST_CODE);
        dialogFragment.show(ft, "dialog");
    }

    public final void showHeightListDialogFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        HeightListDialogFragment dialogFragment = HeightListDialogFragment.Instance((object, position) -> {
            foodPreferenceBeanArrayList = (ArrayList<FoodPreferenceBean>) object;
            setUserHeight();
        });
        dialogFragment.setTargetFragment(null, MY_REQUEST_CODE);
        dialogFragment.show(ft, "dialog");
    }

    public final void showWantKidsDialogFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        WantKidsListDialogFragment dialogFragment = WantKidsListDialogFragment.Instance((object, position) -> {
            foodPreferenceBeanArrayList = (ArrayList<FoodPreferenceBean>) object;
            setWantKids();
        });
        dialogFragment.setTargetFragment(null, MY_REQUEST_CODE);
        dialogFragment.show(ft, "dialog");
    }

    public final void showHaveKidsDialogFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        WantKidsListDialogFragment dialogFragment = WantKidsListDialogFragment.Instance((object, position) -> {
            foodPreferenceBeanArrayList = (ArrayList<FoodPreferenceBean>) object;
            setHaveKids();
        });
        dialogFragment.setTargetFragment(null, MY_REQUEST_CODE);
        dialogFragment.show(ft, "dialog");
    }

    public final void showMeritalDialogFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        MeritalStatusDialogFragment dialogFragment = MeritalStatusDialogFragment.Instance((object, position) -> {
            foodPreferenceBeanArrayList = (ArrayList<FoodPreferenceBean>) object;
            setMeritalStatus();
        });
        dialogFragment.setTargetFragment(null, MY_REQUEST_CODE);
        dialogFragment.show(ft, "dialog");
    }

    private void setMeritalStatus() {
        String name = null;
        for (FoodPreferenceBean bean : foodPreferenceBeanArrayList) {
            if (bean.getIsCheck()) {
                if (name == null) {
                    name = bean.getFoodName();
                } else {
                    name = name + ", " + bean.getFoodName();
                }
            }

        }
        txt_merital_status_upa.setText(name);
    }

    private void setHaveKids() {
        String name = null;
        for (FoodPreferenceBean bean : foodPreferenceBeanArrayList) {
            if (bean.getIsCheck()) {
                if (name == null) {
                    name = bean.getFoodName();
                } else {
                    name = name + ", " + bean.getFoodName();
                }
            }

        }
        txt_have_kids_upa.setText(name);
    }

    private void setWantKids() {
        String name = null;
        for (FoodPreferenceBean bean : foodPreferenceBeanArrayList) {
            if (bean.getIsCheck()) {
                if (name == null) {
                    name = bean.getFoodName();
                } else {
                    name = name + ", " + bean.getFoodName();
                }
            }

        }
        txt_want_kids_upd.setText(name);
    }

    private void setUserHeight() {
        String name = null;
        for (FoodPreferenceBean bean : foodPreferenceBeanArrayList) {
            if (bean.getIsCheck()) {
                if (name == null) {
                    name = bean.getFoodName();
                } else {
                    name = name + ", " + bean.getFoodName();
                }
            }

        }
        txt_height_upa.setText(name);
    }

    private void setCityName() {
        String name = null;
        for (FoodPreferenceBean bean : foodPreferenceBeanArrayList) {
            if (bean.getIsCheck()) {
                if (name == null) {
                    name = bean.getFoodName();
                } else {
                    name = name + ", " + bean.getFoodName();
                }
            }

        }
        txt_city_upa.setText(name);
    }

    private void setFoodChoiceName() {
        String name = null;
        for (FoodPreferenceBean bean : foodPreferenceBeanArrayList) {
            if (bean.getIsCheck()) {
                if (name == null) {
                    name = bean.getFoodName();
                } else {
                    name = name + ", " + bean.getFoodName();
                }
            }

        }
        txt_food_choice_upa.setText(name);
    }

    private void setFoodpreferenceName() {
        String name = null;
        for (FoodPreferenceBean bean : foodPreferenceBeanArrayList) {
            if (bean.getIsCheck()) {
                if (name == null) {
                    name = bean.getFoodName();
                } else {
                    name = name + ", " + bean.getFoodName();
                }
            }

        }
        txt_food_pref_upa.setText(name);
    }

    private UpdateProfileRequest updateProfileRequest;
    private UpdateProfileRequest.Data updateProfileRequestData;

    private void postUpdateProfileApiCall() {
        updateProfileRequest.setData(updateProfileRequestData);
        ApiCall.getInstance().postUpdateProfileApiCall(this, updateProfileRequest, true, this::OnUpdateProfileResponse);
    }

    private void OnUpdateProfileResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    private void opencameraPopup() {
        PopupUtils.getInstance().showCameraGelleryDailog(this, new CameraListener() {
            @Override
            public void onCameraSelect() {
                openCamera();
            }

            @Override
            public void onGallerySelect() {
                openGallery();
            }
        });
    }

    private void openvideoPopup() {
        PopupUtils.getInstance().showVideoGelleryDailog(this, new CameraListener() {
            @Override
            public void onCameraSelect() {
                openVideo();
            }

            @Override
            public void onGallerySelect() {
                openVideoGallery();
            }
        });
    }

    private void openVideoGallery() {
        // for open explorer to select image
        file = null;
        Intent intent;
        if (Build.VERSION.SDK_INT < 19) {
            intent = new Intent();
            intent.setType("video/*");
            intent.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(intent, Constant.APP_NAME), SELECT_VIDEO);
        } else {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("video/*");
            startActivityForResult(Intent.createChooser(intent, Constant.APP_NAME), SELECT_VIDEO);
        }

    }

    private void openVideo() {
        Utilities.getInstance().startRecordingVideo(this);
    }

    Intent intentData;
    private String realPath;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_PHOTO && resultCode == RESULT_OK) {
            realPath = file.getAbsolutePath();
            setCoverImage();

        } else if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && null != data) {
            intentData = data;
            setGalleryView();

        } else if (requestCode == VIDEO_CAPTURE) {

            makeToast("Video");

        } else if (requestCode == SELECT_VIDEO) {

            makeToast("Video select");
        }

    }

    File ImageFile = null;
    private UploadImage uploadImageProfile = new UploadImage();

    private void postEditProfileImage() {

        for(int x=0; x< selectedImage.size(); x++){
            ImageFile = new File(selectedImage.get(x));
            RequestBody ImageBody = RequestBody.create(MediaType.parse("multipart/form-data"), ImageFile);
            MultipartBody.Part iFile = MultipartBody.Part.createFormData("image1", ImageFile.getName(), ImageBody);
            if(x==0)
            uploadImageProfile.setProfilePic(iFile);
        }
        ApiCall.getInstance().postImageUploadApiCall(this, uploadImageProfile, true, this::onImageGenrate);
    }

    List<String> imageList = new ArrayList<>();

    private void onImageGenrate(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {
        if (isSuccess) {
            UploadImageResponse uploadImage = (UploadImageResponse) o;
            imageList.clear();
            imageList.add(uploadImage.getData().getImage1());
            updateProfileRequestData.setImage(imageList);
            updateProfileRequestData.setProfile_image(imageList.get(0));
            postUpdateProfileApiCall();
        }
    }

    private Bitmap bitmap;

    private void setGalleryView() {
        realPath = RealPathUtil.getRealPathFromURI_API19(this, intentData.getData());
        try {
//            bitmap = Bitmap.createBitmap(400, 400, Bitmap.Config.ARGB_8888);
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(realPath));

        } catch (IOException e) {
            e.printStackTrace();
        }
        setCoverImage();

    }

    ArrayList<String> selectedImage;

    private void setCoverImage() {

        if (selectedImage == null)
            selectedImage = new ArrayList<>();
        selectedImage.add(realPath.toString());
        Log.e(ApplicationContext.TAG, realPath.toString());
//        img_profile_maa.setImageURI(Uri.parse(realPath));
//        postEditProfileImage();
        setImageUdapter();
    }


    private ImageListAdapter imageListAdapter;

    private void setImageUdapter() {
        showImageListView();
        imageListAdapter = new ImageListAdapter(this, selectedImage);
        rcv_image_upa.setAdapter(imageListAdapter);
    }

    private void showImageListView() {
        txt_no_record_image_upa.setText("");

    }

    private void hideImageListView() {
        txt_no_record_image_upa.setText(getString(R.string.no_camera_record_yet));
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(UpdateProfileActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(ApplicationContext.TAG, "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });

        } else {
            Log.i(ApplicationContext.TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    /**
     * Provides a simple way of getting a device's location and is well suited for
     * applications that do not require a fine-grained location and that do not need location
     * updates. Gets the best and most recent location currently available, which may be null
     * in rare cases when a location is not available.
     * <p>
     * Note: this method should be called after location permission has been granted.
     */
    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            getLocalityByLatLong(mLastLocation);

//                            mLatitudeText.setText(String.format(Locale.ENGLISH, "%s: %f",
//                                    mLatitudeLabel,
//                                    mLastLocation.getLatitude()));
//                            mLongitudeText.setText(String.format(Locale.ENGLISH, "%s: %f",
//                                    mLongitudeLabel,
//                                    mLastLocation.getLongitude()));
                        } else {
                            Log.w(ApplicationContext.TAG, "getLastLocation:exception", task.getException());
                            showSnackbar(getString(R.string.no_location_detected));
                        }
                    }
                });
    }

    private void getLocalityByLatLong(Location mLastLocation) {
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            String locality = addresses.get(0).getLocality();
            if (submitAddBean == null)
                submitAddBean = new SubmitAddBean();
            submitAddBean.setLocality(addresses.get(0).getLocality());
            submitAddBean.setLatitude(String.valueOf(addresses.get(0).getLatitude()));
            submitAddBean.setLongitude(String.valueOf(addresses.get(0).getLongitude()));
            submitAddBean.setSubLocality(String.valueOf(addresses.get(0).getSubLocality()));
            submitAddBean.setAddress1(String.valueOf(addresses.get(0).getLocality()));
            submitAddBean.setAddress1(String.valueOf(addresses.get(0).getSubLocality()));
            submitAddBean.setZip(String.valueOf(addresses.get(0).getPostalCode()));
//            if (addresses.get(0).getAddressLine(0) == null ||
//                    addresses.get(0).getAddressLine(0).isEmpty()) {
            txt_loc_upa.setText(locality);
//            } else {
//                txt_loc_upa.setText(addresses.get(0).getAddressLine(0));
//
//            }
//            txt_loc_upa.setText(locality + ", " + mLastLocation.getLatitude() + " , " + mLastLocation.getLongitude());
        }

    }


    /**
     * Shows a {@link Snackbar} using {@code text}.
     *
     * @param text The Snackbar text.
     */

    private void showSnackbar(final String text) {
        View container = findViewById(R.id.update_profile_container);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }


    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

}
