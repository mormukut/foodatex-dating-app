package com.mms.foodatex.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.mms.base.BaseAppCompatActivity;
import com.mms.base.ErrorType;
import com.mms.foodatex.ApplicationContext;
import com.mms.foodatex.R;
import com.mms.foodatex.util.PrefSetup;
import com.mms.web_services.ApiCall;
import com.mms.web_services.requestBean.UserInfoRequest;
import com.mms.web_services.responseBean.UserInfoResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

public class UserProfileActivity extends BaseAppCompatActivity {

    @BindView(R.id.profile_img)
    ImageView profile_img;

    @BindView(R.id.txt_info_uia)
    TextView txt_info_uia;

    @BindView(R.id.progress_bar_pa)
    ProgressBar progress_bar_pa;

    @OnClick(R.id.btn_logout_upa)
    void click_btn_logout_upa() {
        userLogout();
    }

    @OnClick(R.id.ll_editprofile_upa)
    void click_ll_editprofile_upa() {
        userProfile();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeView();
    }

    @Override
    public void initializeView() {
        super.initializeView();
        postUserInfoApiCall();
    }

    private UserInfoRequest userInfoRequest;

    private void postUserInfoApiCall() {
        if (userInfoRequest == null)
            userInfoRequest = new UserInfoRequest();
        UserInfoRequest.Data data = new UserInfoRequest.Data();
        data.setUser_id(PrefSetup.getInstance().getUserInfo().getData().getUser_id());
        userInfoRequest.setData(data);
        ApiCall.getInstance().postUserInfoApiCall(this, userInfoRequest, true, this::OnUserInfoResponse);
    }

    private UserInfoResponse userInfoResponse;

    private void OnUserInfoResponse(boolean isSuccess, Response<Object> response, Throwable throwable, ErrorType errorType, Object o) {

        if (isSuccess) {
            userInfoResponse = (UserInfoResponse) o;
            uploadUserInfo();

        }
    }

    private void uploadUserInfo() {
        ApplicationContext.getInstance().loadImage(userInfoResponse.getData().getProfile_image(), profile_img, progress_bar_pa, R.drawable.ic_launcher_background);
        txt_info_uia.setText(userInfoResponse.getData().getFullname() + " , " + userInfoResponse.getData().getDob());
    }

    AccessToken accessToken;

    public void userLogout() {
        accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null)
            LoginManager.getInstance().logOut();
        PrefSetup.getInstance().clearPrefSetup();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    public void userProfile() {
        startActivity(new Intent(this, UpdateProfileActivity.class));
    }
}
