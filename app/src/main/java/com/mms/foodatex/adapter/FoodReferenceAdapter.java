package com.mms.foodatex.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mms.foodatex.R;
import com.mms.foodatex.bean.FoodPreferenceBean;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Admin on 8/25/2018.
 */

public class FoodReferenceAdapter extends RecyclerView.Adapter<FoodReferenceAdapter.MyHolder> {
    Context context;
    ArrayList<FoodPreferenceBean> itemList;

    public FoodReferenceAdapter(Context context, ArrayList<FoodPreferenceBean> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.food_preference_row, parent, false);
        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        holder.checkbox_foodname.setText(itemList.get(position).getFoodName());
        holder.checkbox_foodname.setChecked(itemList.get(position).getIsCheck());
        holder.checkbox_foodname.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                FoodPreferenceBean bean = new FoodPreferenceBean();
                bean.setFoodName(itemList.get(position).getFoodName());
                bean.setIsCheck(b);
                itemList.set(position, bean);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.checkbox_foodname)
        CheckBox checkbox_foodname;

        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
