package com.mms.foodatex.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mms.foodatex.ApplicationContext;
import com.mms.foodatex.R;
import com.mms.web_services.responseBean.UserInfoResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Admin on 8/25/2018.
 */

public class HomeProfileMultiImageAdapter extends RecyclerView.Adapter<HomeProfileMultiImageAdapter.MyHolder> {
    Context context;
    List<UserInfoResponse.Images> itemList;

    public HomeProfileMultiImageAdapter(Context context, List<UserInfoResponse.Images> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_profile_image_row, parent, false);
        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        ApplicationContext.getInstance().loadImage(itemList.get(position).getImage(), holder.img_profile_hpma,
                holder.progress_bar_hpma, R.drawable.ic_launcher_foreground);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_profile_hpma)
        ImageView img_profile_hpma;

        @BindView(R.id.progress_bar_hpma)
        ProgressBar progress_bar_hpma;

        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
