package com.mms.foodatex.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.mms.foodatex.R;
import com.mms.foodatex.bean.FoodPreferenceBean;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Admin on 8/25/2018.
 */

public class RadioButtonAdapter extends RecyclerView.Adapter<RadioButtonAdapter.MyHolder> {
    Context context;
    ArrayList<FoodPreferenceBean> itemList;

    public RadioButtonAdapter(Context context, ArrayList<FoodPreferenceBean> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.radio_group_row, parent, false);
        return new MyHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        holder.radiobutton_checkbox_rba.setText(itemList.get(position).getFoodName());
        holder.radiobutton_checkbox_rba.setChecked(itemList.get(position).getIsCheck());
        holder.radiobutton_checkbox_rba.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                FoodPreferenceBean bean = new FoodPreferenceBean();
                bean.setFoodName(itemList.get(position).getFoodName());
                bean.setIsCheck(b);
                itemList.set(position, bean);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.radiobutton_checkbox_rba)
        RadioButton radiobutton_checkbox_rba;

        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
