package com.mms.foodatex.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mms.base.OnListItemClickListener;
import com.mms.foodatex.ApplicationContext;
import com.mms.foodatex.R;
import com.mms.foodatex.activity.HomeProfielDetailsActivity;
import com.mms.web_services.responseBean.SearchUserResponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Admin on 8/25/2018.
 */

public class UserSwipeListAdapter implements Adapter {
    Activity context;
    List<SearchUserResponse.Data> itemList;
    OnListItemClickListener listener;

    public UserSwipeListAdapter(Activity context, List<SearchUserResponse.Data> itemList, OnListItemClickListener listener) {
        this.context = context;
        this.itemList = itemList;
        this.listener = listener;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.user_swipe_row, null, true);
        ImageView profilePic = rowView.findViewById(R.id.img_profile_user_pa);
        TextView txt_info_pa = rowView.findViewById(R.id.txt_info_pa);
        TextView txt_address = rowView.findViewById(R.id.txt_address);
        TextView txt_type = rowView.findViewById(R.id.txt_type);
        CardView crd_main_usla = rowView.findViewById(R.id.crd_main_usla);
        CardView crd_inner_usla = rowView.findViewById(R.id.crd_inner_usla);
        ViewTreeObserver vto = crd_main_usla.getViewTreeObserver();
        ViewTreeObserver vto_inner = crd_inner_usla.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    crd_main_usla.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    crd_main_usla.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                int width = crd_main_usla.getMeasuredWidth();
                int height = crd_main_usla.getMeasuredHeight();
                ViewGroup.LayoutParams params = crd_main_usla.getLayoutParams();
// Changes the height and width to the specified *pixels*
                params.height = width + 200;
                params.width = width;
                crd_main_usla.setLayoutParams(params);

            }
        });
        vto_inner.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    crd_inner_usla.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    crd_inner_usla.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                int width = crd_inner_usla.getMeasuredWidth();
//                int height = crd_inner_usla.getMeasuredHeight();
                ViewGroup.LayoutParams params = crd_inner_usla.getLayoutParams();
// Changes the height and width to the specified *pixels*
                params.height = width;
                params.width = width;
                crd_inner_usla.setLayoutParams(params);

            }
        });

        ProgressBar progressBar = rowView.findViewById(R.id.progress_bar_pa);
        try {
            txt_info_pa.setText(itemList.get(i).getFullname());
            txt_address.setText(itemList.get(i).getAddress());
            txt_type.setText(itemList.get(i).getFood_preference() == null || TextUtils.isEmpty(itemList.get(i).getFood_preference()) ? "-" : itemList.get(i).getFood_preference());
            ApplicationContext.getInstance().loadImage(itemList.get(i).getProfile_image(), profilePic, progressBar, R.drawable.ic_launcher_foreground);
            profilePic.setOnClickListener(view1 -> {
                listener.onItemClick(itemList.get(i), i);

                Intent intent = new Intent(context, HomeProfielDetailsActivity.class);
                intent.putExtra("Data", itemList.get(i));
                context.startActivity(intent);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowView;
    }

    @Override
    public int getItemViewType(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
