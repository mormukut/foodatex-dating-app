package com.mms.foodatex.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 8/25/2018.
 */

public class FoodPreferenceBean {


    @Expose
    @SerializedName("isCheck")
    private boolean isCheck;
    @Expose
    @SerializedName("FoodName")
    private String FoodName;

    public boolean getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(boolean isCheck) {
        this.isCheck = isCheck;
    }

    public String getFoodName() {
        return FoodName;
    }

    public void setFoodName(String FoodName) {
        this.FoodName = FoodName;
    }
}
