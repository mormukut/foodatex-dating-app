package com.mms.foodatex.fragment.FoodPreference;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mms.base.DoActionType;
import com.mms.base.MainView;
import com.mms.base.OnListItemClickListener;
import com.mms.foodatex.R;
import com.mms.foodatex.adapter.FoodReferenceAdapter;
import com.mms.foodatex.adapter.RadioButtonAdapter;
import com.mms.foodatex.bean.FoodPreferenceBean;
import com.mms.utilities.Utilities;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HeightListDialogFragment extends DialogFragment implements MainView {

    private static OnListItemClickListener mylistener;
    @BindView(R.id.rcv_foodList_fpd)
    RecyclerView rcv_foodList_fpd;

    @BindView(R.id.txt_heading_fpd)
    TextView txt_heading_fpd;

    @BindView(R.id.btn_done_action_fpd)
    Button btn_done_action_fpd;

    @BindView(R.id.ll_radio_cldf)
    LinearLayout ll_radio_cldf;

    @OnClick(R.id.btn_done_action_fpd)
    void click_btn_done_action_fpd() {
        if (isValid()) {
            mylistener.onItemClick(foodNameList, 0);
            dismiss();
        }
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(checkposition)) {
            Toast.makeText(getActivity(), "Select at least one food", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            for (int i = 0; i < foodNameList.size(); i++) {
                foodNameList.get(i).setIsCheck(checkposition.equals(foodNameList.get(i).getFoodName()) ? true : false);
            }
            return true;
        }
    }

    ArrayList<FoodPreferenceBean> foodNameList;

    View fragmentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_food_preference_dialog, container, false);
        ButterKnife.bind(this, fragmentView);
        initializeView();
        return fragmentView;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public static HeightListDialogFragment fragment = new HeightListDialogFragment();

    public static HeightListDialogFragment Instance(OnListItemClickListener listener) {
        mylistener = listener;
        return fragment;
    }

    @Override
    public void initializeView() {
        addListData();
        ll_radio_cldf.removeAllViews();
        Utilities.getInstance().setRecyclerViewUISpanCount(getActivity(), rcv_foodList_fpd, 1, DoActionType.Vertical);
        setAdapter();
        txt_heading_fpd.setText(getString(R.string.select_your_height));
    }

    private FoodPreferenceBean foodPreferenceBean;

    private void addListData() {
        if (foodNameList != null)
            return;
        foodNameList = new ArrayList<>();

        foodPreferenceBean = new FoodPreferenceBean();
        foodPreferenceBean.setFoodName("5''");
        foodPreferenceBean.setIsCheck(false);
        foodNameList.add(foodPreferenceBean);

        foodPreferenceBean = new FoodPreferenceBean();
        foodPreferenceBean.setFoodName("6''");
        foodPreferenceBean.setIsCheck(false);
        foodNameList.add(foodPreferenceBean);

        foodPreferenceBean = new FoodPreferenceBean();
        foodPreferenceBean.setFoodName("7''");
        foodPreferenceBean.setIsCheck(false);
        foodNameList.add(foodPreferenceBean);
    }


    String checkposition;

    private void setAdapter() {
        RadioGroup radioGroup = Utilities.getInstance().setRadioGroup(getActivity(), foodNameList);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                checkposition = String.valueOf(radioGroup.findViewById(i).getTag());
            }
        });

        ll_radio_cldf.addView(radioGroup);
    }


//    private RadioButtonAdapter adapter;
//
//    private void setAdapter() {
//        if (adapter == null)
//            adapter = new RadioButtonAdapter(getActivity(), foodNameList);
//        rcv_foodList_fpd.setAdapter(adapter);
//
//    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }
}
