package com.mms.foodatex.fragment.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.CompoundButton;

import com.mms.base.BaseAppCompatActivity;
import com.mms.base.MainView;
import com.mms.foodatex.ApplicationContext;

/**
 * Created by mormukutsinghji@gmail.com on 3/14/2018.
 */

public class BaseFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, MainView {

    public int fragmentType;
    public BaseAppCompatActivity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseAppCompatActivity) {
            activity = (BaseAppCompatActivity) context;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        ApplicationContext.getInstance().setLiveFragment(this);
    }


    @Override
    public void initializeView() {

    }

    @Override
    public void showProgressBar() {
        activity.progressON();
    }


    @Override
    public void hideProgressBar() {
        activity.progressOFF();
    }


    @Override
    public void onClick(View arg0) {
        // TODO Auto-generated method stub
//		Intent i  = null;
        switch (arg0.getId()) {

        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // TODO Auto-generated method stub

    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void makeToast(String message) {
        activity.makeToast(message);
    }
}
