package com.mms.foodatex.fragment.base;

/**
 * Created by admin on 23/7/2015.
 */
public enum FragmentNames {
    LoginFragment(0),
    ForgotPasswordFragment(1),
    WelcomeFragment(2),
    DashBoardFragment(3);
    private final int value;

    FragmentNames(final int newValue) {
        value = newValue;
    }

    public int getValue() {
        return value;
    }

}
