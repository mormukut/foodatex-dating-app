package com.mms.foodatex.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.mms.foodatex.ApplicationContext;
import com.mms.utilities.Constant;
import com.mms.web_services.responseBean.LoginResponse;

/**
 * Created by mormukutsinghji@gmail.com on 3/14/2018.
 */

public class PrefSetup {

    private static PrefSetup instance;

    private PrefSetup(Context context) {
        sp = context.getSharedPreferences(Constant.PREF_FILE, Context.MODE_PRIVATE);
    }

    public static synchronized PrefSetup getInstance() {
        if (instance != null)
            return instance;

        instance = new PrefSetup(ApplicationContext.getInstance());
        return instance;
    }

    public SharedPreferences sp;

    private String UserDetails = "userdetails";
    private String UserInfo = "userInfo";
    private String UserId = "userId";
    private String ProfileImage = "profileImage";
    private String UserName = "userName";
    private String FilterCategory = "filterCategory";
    private String FilterTag = "filterTag";
    String json;


    public LoginResponse getUserInfo() {
        Gson gson = new Gson();
        LoginResponse userDetail = new LoginResponse();
        if (sp.getString(UserDetails, "") != null) {
            json = sp.getString(UserInfo, "");
            return gson.fromJson(json, LoginResponse.class);
        } else {
            return userDetail;
        }
    }
//
//    public ShortingProductResponse.Categories getFilterCategory() {
//        Gson gson = new Gson();
//        ShortingProductResponse.Categories categories = new ShortingProductResponse.Categories();
//        if (sp.getString(UserDetails, "") != null) {
//            json = sp.getString(FilterCategory, "");
//            return gson.fromJson(json, ShortingProductResponse.Categories.class);
//        } else {
//            return categories;
//        }
//    }
//
//    public ShortingProductResponse.Tags getFilterTag() {
//        Gson gson = new Gson();
//        ShortingProductResponse.Tags tags = new ShortingProductResponse.Tags();
//        if (sp.getString(UserDetails, "") != null) {
//            json = sp.getString(FilterTag, "");
//            return gson.fromJson(json, ShortingProductResponse.Tags.class);
//        } else {
//            return tags;
//        }
//    }
//    public UserLoginResponse getUserDetail() {
//        Gson gson = new Gson();
//        UserLoginResponse userDetail = new UserLoginResponse();
//        if (sp.getString(UserDetails, "") != null) {
//            json = sp.getString(UserDetails, "");
//            return gson.fromJson(json, UserLoginResponse.class);
//        } else {
//            return userDetail;
//        }
//    }

    public String getUserId() {
        return sp.getString(UserId, null);
    }

    public String getUserName() {
        return sp.getString(UserName, null);
    }

    public String getProfileImage() {
        return sp.getString(ProfileImage, null);
    }


    public void setUserInfo(LoginResponse userInfo) {
        SharedPreferences.Editor editor = sp.edit();
        Gson gson = new Gson();
        String json = gson.toJson(userInfo);
        editor.putString(UserInfo, json);
        editor.commit();
    }
//    public void setUserDetail(UserLoginResponse userDetail) {
//        SharedPreferences.Editor editor = sp.edit();
//        Gson gson = new Gson();
//        String json = gson.toJson(userDetail);
//        editor.putString(UserDetails, json);
//        editor.commit();
//    }

//    public void setFilterCategory(ShortingProductResponse.Categories filterCategory) {
//        SharedPreferences.Editor editor = sp.edit();
//        Gson gson = new Gson();
//        String json = gson.toJson(filterCategory);
//        editor.putString(FilterCategory, json);
//    }

//    public void setFilterTag(ShortingProductResponse.Tags filterTag) {
//        SharedPreferences.Editor editor = sp.edit();
//        Gson gson = new Gson();
//        String json = gson.toJson(filterTag);
//        editor.putString(FilterTag, json);
//    }

    public void setUserId(String userId) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(this.UserId, userId);
        editor.commit();
    }

    public void setProfileImage(String userId) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(this.ProfileImage, userId);
        editor.commit();
    }

    public void setUserName(String userName) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(this.UserName, userName);
        editor.commit();
    }

    public void clearPrefSetup() {
        sp.edit().clear().commit();
    }

}
