package com.mms.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.mms.utilities.FontCache;


public class BoldButtonText extends Button {
    public BoldButtonText(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public BoldButtonText(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public BoldButtonText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("Fonts/Chewy.ttf", context);
//        Typeface customFont = FontCache.getTypeface("Fonts/Courgette-Regular.ttf", context);
//        Typeface customFont = FontCache.getTypeface("Fonts/GOTHIC.ttf", context);
        setTypeface(customFont);
    }
}
