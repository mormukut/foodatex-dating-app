package com.mms.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.mms.utilities.FontCache;


public class RegularEditText extends EditText {
    public RegularEditText(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public RegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public RegularEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
//        Typeface customFont = FontCache.getTypeface("Fonts/Chewy.ttf", context);
//        Typeface customFont = FontCache.getTypeface("Fonts/Courgette-Regular.ttf", context);
        Typeface customFont = FontCache.getTypeface("Fonts/GOTHIC.ttf", context);
        setTypeface(customFont);
    }
}
