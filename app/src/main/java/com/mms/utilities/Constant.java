package com.mms.utilities;

/**
 * Created by Admin on 3/14/2018.
 */

public interface Constant {

    String APP_NAME = "Foodatex";
    String PREF_FILE = "App_Pref";
    String ERROR_REPORT_EMAIL = "mormukutsinghji@gmail.com";
    String ERROR_REPORT_TITLE = "Crash Report " + APP_NAME + " (Report to Developer)";
    String RECEIVE_Notification = "RECEIVE_Notification";
    String RECEIVE_ON_REQUEST_DETAIL = "receive on request detail";
    String NO_IMAGE_FOUND = "https://www.icicilombard.com/Content/ilom-en/Rural%20Insurance/Weavers/weaver_banner.jpg";
//    String NO_IMAGE_FOUND = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZqXEnhL1ztH3_H9d4bjsz6tamLqji90b58q6DixeS67YoxyF5";

    String PLACE_API_BASE = "https://maps.googleapis.com/maps/api/place";
    String TYPE_AUTOCOMPLETE = "/autocomplete";
    String OUT_JSON = "/json";
        String API_KEY = "AIzaSyBuM-2g8SLx5HFMpRI9EH7M06M2BYvJRCo";
//    String API_KEY = "e5c27a774848d988ff604c758bbc485413a3727d";
    String Content_Type = "application/x-www-form-urlencoded";
    int VIDEO_CAPTURE = 1008;
    int SELECT_PHOTO = 101;
    int SELECT_VIDEO = 102;
    int CAPTURE_PHOTO = 100;
}
