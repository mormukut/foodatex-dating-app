package com.mms.web_services;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mms.base.BaseAppCompatActivity;
import com.mms.base.ErrorType;
import com.mms.foodatex.ApplicationContext;
import com.mms.foodatex.R;
import com.mms.foodatex.util.PopupUtils;
import com.mms.foodatex.util.PrefSetup;
import com.mms.web_services.listener.APIService;
import com.mms.web_services.listener.PostResponseListener;
import com.mms.web_services.requestBean.ForgotPasswordRequest;
import com.mms.web_services.requestBean.LoginRequest;
import com.mms.web_services.requestBean.RegistrationRequest;
import com.mms.web_services.requestBean.SearchUserRequest;
import com.mms.web_services.requestBean.SocialLoginRequest;
import com.mms.web_services.requestBean.SwipeRequest;
import com.mms.web_services.requestBean.UpdateProfileRequest;
import com.mms.web_services.requestBean.UploadImage;
import com.mms.web_services.requestBean.UserInfoRequest;
import com.mms.web_services.requestBean.UserListNearByRequest;
import com.mms.web_services.responseBean.LoginResponse;
import com.mms.web_services.responseBean.RegistrationResponse;
import com.mms.web_services.responseBean.SearchUserResponse;
import com.mms.web_services.responseBean.SwipeUserResponse;
import com.mms.web_services.responseBean.UploadImageResponse;
import com.mms.web_services.responseBean.UserInfoResponse;
import com.mms.web_services.responseBean.UserListNearByResponse;

import retrofit2.Call;

/**
 * Created by mormukutsinghji@gmail.com on 3/15/2018.
 */

public class ApiCall {

    APIService service;


    private static final ApiCall ourInstance = new ApiCall();

    public static ApiCall getInstance() {
        return ourInstance;
    }

    private ApiCall() {
    }

    private void setBasicInfo(int connectionTimeOut, int readTimeOut) {
        Log.e(ApplicationContext.TAG, "click");
        service = RetrofitBuilder.getInstance().getRetrofitInstance(connectionTimeOut, readTimeOut).create(APIService.class);
    }

    public void postUserLoginApiCall(BaseAppCompatActivity activity, LoginRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(100, 100);

        Call<Object> call = service.postUserLogin(request);

        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {

            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    LoginResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), LoginResponse.class);
                    if (user != null && user.getStatus().equals("1")) {
                        Log.e(ApplicationContext.TAG, user.toString());
//                        PrefSetup.getInstance().setUserInfo(user);
                        callBack.onSuccess(true, response, t, errorType, null);
                    } else if (user != null && user.getStatus().equals("0")) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                user.getMessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                            jsonObject1.getAsJsonObject("body") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("message") != null ?
                                    jsonObject1.getAsJsonObject("body").get("message").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postUserLoginApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postSocialLoginApiCall(BaseAppCompatActivity activity, SocialLoginRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(100, 100);

        Call<Object> call = service.postSocialLogin(request);

        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {

            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    LoginResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), LoginResponse.class);
                    if (user != null && user.getStatus().equals("1")) {
                        Log.e(ApplicationContext.TAG, user.toString());
                        PrefSetup.getInstance().setUserInfo(user);
                        callBack.onSuccess(true, response, t, errorType, null);
                    } else if (user != null && user.getStatus().equals("0")) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                user.getMessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                            jsonObject1.getAsJsonObject("body") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("message") != null ?
                                    jsonObject1.getAsJsonObject("body").get("message").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postSocialLoginApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postSearchUserApiCall(BaseAppCompatActivity activity, SearchUserRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(100, 100);

        Call<Object> call = service.postSearchUser(request);

        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {

            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    SearchUserResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), SearchUserResponse.class);
                    if (user != null && user.getStatus().equals("1")) {
                        Log.e(ApplicationContext.TAG, user.toString());
                        callBack.onSuccess(true, response, t, errorType, user);
                    } else if (user != null && user.getStatus().equals("0")) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                user.getMessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                            jsonObject1.getAsJsonObject("body") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("message") != null ?
                                    jsonObject1.getAsJsonObject("body").get("message").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postSearchUserApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postUserInfoApiCall(BaseAppCompatActivity activity, UserInfoRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(100, 100);

        Call<Object> call = service.postUserDetails(request);

        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {

            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    UserInfoResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), UserInfoResponse.class);
                    if (user != null && user.getStatus().equals("1")) {
                        Log.e(ApplicationContext.TAG, user.toString());
                        callBack.onSuccess(true, response, t, errorType, user);
                    } else if (user != null && user.getStatus().equals("0")) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                user.getMessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                            jsonObject1.getAsJsonObject("body") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("message") != null ?
                                    jsonObject1.getAsJsonObject("body").get("message").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postUserInfoApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postUserSwipeApiCall(BaseAppCompatActivity activity, SwipeRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(100, 100);

        Call<Object> call = service.postSwipeUser(request);

        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {

            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    SwipeUserResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), SwipeUserResponse.class);
                    if (user != null && user.getStatus().equals("1")) {
                        Log.e(ApplicationContext.TAG, user.toString());
                        callBack.onSuccess(true, response, t, errorType, user);
                    } else if (user != null && user.getStatus().equals("0")) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                user.getMessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                            jsonObject1.getAsJsonObject("body") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("message") != null ?
                                    jsonObject1.getAsJsonObject("body").get("message").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postUserSwipeApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postUserListNearByApiCall(BaseAppCompatActivity activity, UserListNearByRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(100, 100);

        Call<Object> call = service.postUserListNearBy(request);

        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {

            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    UserListNearByResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), UserListNearByResponse.class);
                    if (user != null && user.getStatus().equals("1")) {
                        Log.e(ApplicationContext.TAG, user.toString());
                        callBack.onSuccess(true, response, t, errorType, null);
                    } else if (user != null && user.getStatus().equals("0")) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                user.getMessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                            jsonObject1.getAsJsonObject("body") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("message") != null ?
                                    jsonObject1.getAsJsonObject("body").get("message").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postUserListNearByApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

//    public void postSearchUserApiCall(BaseAppCompatActivity activity, SearchUserRequest request, boolean showDialog, PostResponseListener callBack) {
//        setBasicInfo(100, 100);
//
//        Call<Object> call = service.postSearchUser(request);
//
//        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
//
//            if (isSuccess && response != null) {
//                Gson gson = new Gson();
//                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
//                try {
//                    LoginResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), LoginResponse.class);
//                    if (user != null && user.getStatus().equals("1")) {
//                        Log.e(ApplicationContext.TAG, user.toString());
//                        PrefSetup.getInstance().setUserInfo(user);
//                        callBack.onSuccess(true, response, t, errorType, null);
//                    } else if (user != null && user.getStatus().equals("0")) {
//                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
//                                user.getMessage(), (object, position) -> {
//                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
//                                });
//                    } else {
//                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
//                                activity.getString(R.string.noDataFound), (object, position) -> {
//                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
//                                });
//                    }
//                } catch (Exception e) {
//                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
//                            jsonObject1.getAsJsonObject("body") != null &&
//                                    jsonObject1.getAsJsonObject("body").get("status") != null &&
//                                    jsonObject1.getAsJsonObject("body").get("status").toString().equals("0.0") &&
//                                    jsonObject1.getAsJsonObject("body").get("message") != null ?
//                                    jsonObject1.getAsJsonObject("body").get("message").toString() :
//                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
//                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
//                            });
//                }
//            } else if (errorType == ErrorType.NO_INTERNET) {
//                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
//                        activity.getString(R.string.noInternetAccess), aBoolean -> {
//                            if (aBoolean) {
//                                postSocialLoginApiCall(activity, request, showDialog, callBack);
//                            } else {
//                                activity.onBackPressed();
//                            }
//                        });
//            } else {
//                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
//                        activity.getString(R.string.ERROR), (object, position) -> {
//                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
//                        });
//            }
//        });
//    }

    public void postUpdateProfileApiCall(BaseAppCompatActivity activity, UpdateProfileRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(100, 100);

        Call<Object> call = service.postUpdateProfile(request);

        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {

            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    LoginResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), LoginResponse.class);
                    if (user != null && user.getStatus().equals("1")) {
                        Log.e(ApplicationContext.TAG, user.toString());
//                        PrefSetup.getInstance().setUserInfo(user);
                        callBack.onSuccess(true, response, t, errorType, null);
                    } else if (user != null && user.getStatus().equals("0")) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                user.getMessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                            jsonObject1.getAsJsonObject("body") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("message") != null ?
                                    jsonObject1.getAsJsonObject("body").get("message").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postUpdateProfileApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postUserRegistrationApiCall(BaseAppCompatActivity activity, RegistrationRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(100, 100);
        Call<Object> call = service.postUserRegistration(request);

        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {

            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    RegistrationResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), RegistrationResponse.class);
                    if (user != null && user.getStatus().equals("1")) {
                        Log.e(ApplicationContext.TAG, user.toString());
//                        PrefSetup.getInstance().setUserInfo(user);
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                user.getMessage(), (object, position) -> {
                                    callBack.onSuccess(true, response, t, errorType, null);
                                });

                    } else if (user != null && user.getStatus().equals("0")) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                user.getMessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("message") != null ?
                                    jsonObject1.getAsJsonObject("body").get("message").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postUserRegistrationApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }

    public void postForgotPasswordApiCall(BaseAppCompatActivity activity, ForgotPasswordRequest request, boolean showDialog, PostResponseListener callBack) {
        setBasicInfo(100, 100);
        Call<Object> call = service.postForgotPassword(request);

        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {

            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    RegistrationResponse user = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), RegistrationResponse.class);
                    if (user != null && user.getStatus().equals("1")) {
                        Log.e(ApplicationContext.TAG, user.toString());
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                user.getMessage(), (object, position) -> {
                                    callBack.onSuccess(true, response, t, errorType, null);
                                });

                    } else if (user != null && user.getStatus().equals("0")) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                user.getMessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status") != null &&
                                    jsonObject1.getAsJsonObject("body").get("status").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("message") != null ?
                                    jsonObject1.getAsJsonObject("body").get("message").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postForgotPasswordApiCall(activity, request, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.login),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }


    private void setMultipleBasicInfoAndId(int connectionTimeOut, int readTimeOut) {
        Log.e(ApplicationContext.TAG, "click");
        service = RetrofitBuilder.getInstance().getMultipleRetrofitGetID(connectionTimeOut, readTimeOut).create(APIService.class);
    }

    public void postImageUploadApiCall(BaseAppCompatActivity activity, UploadImage iFile, boolean showDialog, PostResponseListener callBack) {
        setMultipleBasicInfoAndId(1000, 1000);
        Call<Object> call = service.postUploadFile(iFile.getProfilePic());
        ApiListener.getInstance().onPostApiCall(activity, call, showDialog, (isSuccess, response, t, errorType, o) -> {
            Log.e(ApplicationContext.TAG, "data");
            if (isSuccess && response != null) {
                Gson gson = new Gson();
                JsonObject jsonObject1 = gson.toJsonTree(response).getAsJsonObject();
                try {
                    UploadImageResponse addToCartResponce = new Gson().fromJson(jsonObject1.getAsJsonObject("body").toString(), UploadImageResponse.class);
                    if (addToCartResponce != null && addToCartResponce.getStatus().equals("1")) {
                        Log.e(ApplicationContext.TAG, addToCartResponce.toString());
                        callBack.onSuccess(true, response, t, errorType, addToCartResponce);
                    } else if (addToCartResponce != null && addToCartResponce.getStatus().equals("0")) {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                addToCartResponce.getMessage(), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    } else {
                        PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                                activity.getString(R.string.noDataFound), (object, position) -> {
                                    callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                                });
                    }
                } catch (Exception e) {
                    PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                            jsonObject1.getAsJsonObject("body") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode") != null &&
                                    jsonObject1.getAsJsonObject("body").get("responsecode").toString().equals("0.0") &&
                                    jsonObject1.getAsJsonObject("body").get("responsemessage") != null ?
                                    jsonObject1.getAsJsonObject("body").get("responsemessage").toString() :
                                    activity.getString(R.string.PARSING_ERROR), (object, position) -> {
                                callBack.onSuccess(false, response, t, ErrorType.PARSING_ERROR, null);
                            });
                }
            } else if (errorType == ErrorType.NO_INTERNET) {
                PopupUtils.getInstance().showRetryDialogNoCancelable(activity, activity.getString(R.string.no_internet_connection),
                        activity.getString(R.string.noInternetAccess), aBoolean -> {
                            if (aBoolean) {
                                postImageUploadApiCall(activity, iFile, showDialog, callBack);
                            } else {
                                activity.onBackPressed();
                            }
                        });
            } else {
                PopupUtils.getInstance().showOkButtonDialogNoCancelable(activity, activity.getString(R.string.alert),
                        activity.getString(R.string.ERROR), (object, position) -> {
                            callBack.onSuccess(false, response, t, ErrorType.ERROR, null);
                        });
            }
        });
    }
}
