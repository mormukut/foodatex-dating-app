package com.mms.web_services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mormukutsinghji@gmail.com on 3/15/2018.
 */

public class RetrofitBuilder {

//    public static final String IP = "http://jobandoffers.com/demo/foodatex/api/Registration/UserRegistration";
    public static final String IP = "http://jobandoffers.com/";
    public static final String BASE_API_URL = IP + "demo/foodatex/api/";
    public static final String LOGIN_URL = "Registration/CheckLogin";
    public static final String REGISTRATION_URL = "Registration/UserRegistration";
    public static final String UPDATE_PROFILE_URL = "Registration/UpdateProfile";
    public static final String FORGOT_PASSWORD_URL = "Registration/ForgotPassword";
    public static final String SEARCH_USER_URL = "Registration/SearchUsers";
    public static final String USER_LIST_NEARBY_URL = "Users/userslistnearby";
    public static final String USER_INFO_URL = "Registration/GetUserDetail";
    public static final String SWIPE_USER_URL = "users/swipeuser";

    public static final String BASE_UPLOAD_FILE_URL = "http://jobandoffers.com/demo/foodatex/";
    public static final String UPLOAD_FILE = "upload/upload.php";

    private Retrofit retrofit;

    private static final RetrofitBuilder ourInstance = new RetrofitBuilder();

    public static RetrofitBuilder getInstance() {
        return ourInstance;
    }

    private RetrofitBuilder() {
    }

    public Retrofit getRetrofitInstance(int connectionTimeOut, int readTimeOut) {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };


            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient client = builder
                    .connectTimeout(connectionTimeOut, TimeUnit.SECONDS)
                    .readTimeout(readTimeOut, TimeUnit.SECONDS).build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_API_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Retrofit getMultipleRetrofitGetID(int connectionTimeOut, int readTimeOut) {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };


            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

//                OkHttpClient.Builder builder = new OkHttpClient.Builder();
//                builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
//                builder.hostnameVerifier(new HostnameVerifier() {
//                    @Override
//                    public boolean verify(String hostname, SSLSession session) {
//                        return true;
//                    }
//                });
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request request = original.newBuilder()
//                            .header("Content-Type", "application/json;charset=UTF-8")
//                            .header("SECRET", "3c3fd386ce22e5f7eb032bbd4018e2f6f4d6f8c4")
//                            .header("USERID", userId)
                            .method(original.method(), original.body())
                            .build();

                    return chain.proceed(request);
                }
            });

            httpClient.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            httpClient.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

//                    OkHttpClient client = httpClient.build();
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = httpClient
                    .connectTimeout(connectionTimeOut, TimeUnit.SECONDS)
                    .readTimeout(readTimeOut, TimeUnit.SECONDS).
                            addInterceptor(interceptor).
                            build();
//                OkHttpClient client = builder
//                        .connectTimeout(connectionTimeOut, TimeUnit.SECONDS)
//                        .readTimeout(readTimeOut, TimeUnit.SECONDS).build();
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_UPLOAD_FILE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            return retrofit;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
