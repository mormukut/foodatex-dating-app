package com.mms.web_services.listener;


import com.mms.web_services.RetrofitBuilder;
import com.mms.web_services.requestBean.ForgotPasswordRequest;
import com.mms.web_services.requestBean.LoginRequest;
import com.mms.web_services.requestBean.RegistrationRequest;
import com.mms.web_services.requestBean.SearchUserRequest;
import com.mms.web_services.requestBean.SocialLoginRequest;
import com.mms.web_services.requestBean.SwipeRequest;
import com.mms.web_services.requestBean.UpdateProfileRequest;
import com.mms.web_services.requestBean.UserInfoRequest;
import com.mms.web_services.requestBean.UserListNearByRequest;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by mormukutsinghji@gmail.com on 3/15/2018.
 */

public interface APIService {

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(RetrofitBuilder.LOGIN_URL)
    Call<Object> postUserLogin(@Body LoginRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(RetrofitBuilder.REGISTRATION_URL)
    Call<Object> postSocialLogin(@Body SocialLoginRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(RetrofitBuilder.REGISTRATION_URL)
    Call<Object> postUserRegistration(@Body RegistrationRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(RetrofitBuilder.FORGOT_PASSWORD_URL)
    Call<Object> postForgotPassword(@Body ForgotPasswordRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(RetrofitBuilder.UPDATE_PROFILE_URL)
    Call<Object> postUpdateProfile(@Body UpdateProfileRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(RetrofitBuilder.SEARCH_USER_URL)
    Call<Object> postSearchUser(@Body SearchUserRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(RetrofitBuilder.USER_LIST_NEARBY_URL)
    Call<Object> postUserListNearBy(@Body UserListNearByRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(RetrofitBuilder.USER_INFO_URL)
    Call<Object> postUserDetails(@Body UserInfoRequest bean);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST(RetrofitBuilder.SWIPE_USER_URL)
    Call<Object> postSwipeUser(@Body SwipeRequest response);

    @Multipart
    @POST(RetrofitBuilder.UPLOAD_FILE)
    Call<Object> postUploadFile(@Part MultipartBody.Part vFile);



}
