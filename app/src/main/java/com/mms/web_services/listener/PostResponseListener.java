package com.mms.web_services.listener;

import com.mms.base.ErrorType;

import retrofit2.Response;

/**
 * Created by mormukutsinghji@gmail.com on 3/15/2018.
 */

public interface PostResponseListener {
    void onSuccess(boolean isSuccess, Response<Object> response, Throwable t, ErrorType errorType, Object o);
}
