package com.mms.web_services.requestBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 8/14/2018.
 */

public class SocialLoginRequest {


    @Expose
    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("fullname")
        private String fullname;
        @Expose
        @SerializedName("social_id")
        private String social_id;
        @Expose
        @SerializedName("logintype")
        private String logintype;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getSocial_id() {
            return social_id;
        }

        public void setSocial_id(String social_id) {
            this.social_id = social_id;
        }

        public String getLogintype() {
            return logintype;
        }

        public void setLogintype(String logintype) {
            this.logintype = logintype;
        }
    }
}
