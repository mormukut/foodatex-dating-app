package com.mms.web_services.requestBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 9/28/2018.
 */

public class SwipeRequest {


    @Expose
    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("swipes")
        private List<Swipes> swipes;
        @Expose
        @SerializedName("sender_u_id")
        private String sender_u_id;

        public List<Swipes> getSwipes() {
            return swipes;
        }

        public void setSwipes(List<Swipes> swipes) {
            this.swipes = swipes;
        }

        public String getSender_u_id() {
            return sender_u_id;
        }

        public void setSender_u_id(String sender_u_id) {
            this.sender_u_id = sender_u_id;
        }
    }

    public static class Swipes {
        @Expose
        @SerializedName("swipe")
        private int swipe;
        @Expose
        @SerializedName("receiver_u_id")
        private String receiver_u_id;

        public int getSwipe() {
            return swipe;
        }

        public void setSwipe(int swipe) {
            this.swipe = swipe;
        }

        public String getReceiver_u_id() {
            return receiver_u_id;
        }

        public void setReceiver_u_id(String receiver_u_id) {
            this.receiver_u_id = receiver_u_id;
        }
    }
}
