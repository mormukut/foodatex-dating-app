package com.mms.web_services.requestBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 8/26/2018.
 */

public class UpdateProfileRequest {

    @Expose
    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("dob")
        private String dob;
        @Expose
        @SerializedName("image")
        private List<String> image;
        @Expose
        @SerializedName("video")
        private List<String> video;
        @Expose
        @SerializedName("phone")
        private String phone;
        @Expose
        @SerializedName("user_id")
        private String user_id;
        @Expose
        @SerializedName("do_you_smoke")
        private String do_you_smoke;
        @Expose
        @SerializedName("looking_for")
        private int looking_for;
        @Expose
        @SerializedName("split_bill")
        private String split_bill;
        @Expose
        @SerializedName("hide_adverts")
        private String hide_adverts;
        @Expose
        @SerializedName("do_you_want_kids")
        private String do_you_want_kids;
        @Expose
        @SerializedName("about_yourself")
        private String about_yourself;
        @Expose
        @SerializedName("are_you_a")
        private String are_you_a;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("any_allergies")
        private String any_allergies;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("any_allergies_status")
        private String any_allergies_status;
        @Expose
        @SerializedName("personality")
        private String personality;
        @Expose
        @SerializedName("longitude")
        private String longitude;
        @Expose
        @SerializedName("distance_invisible")
        private String distance_invisible;
        @Expose
        @SerializedName("fullname")
        private String fullname;
        @Expose
        @SerializedName("dont_show_my_age")
        private String dont_show_my_age;
        @Expose
        @SerializedName("food_preference")
        private String food_preference;
        @Expose
        @SerializedName("country_code")
        private String country_code;
        @Expose
        @SerializedName("unlimited_likes")
        private String unlimited_likes;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("foodatext_boost")
        private String foodatext_boost;
        @Expose
        @SerializedName("marital_status")
        private String marital_status;
        @Expose
        @SerializedName("profile_image")
        private String profile_image;
        @Expose
        @SerializedName("interest_in")
        private int interest_in;
        @Expose
        @SerializedName("allowing_you")
        private String allowing_you;
        @Expose
        @SerializedName("do_you_have_kids")
        private String do_you_have_kids;
        @Expose
        @SerializedName("who_see_you")
        private String who_see_you;
        @Expose
        @SerializedName("what_do_you")
        private String what_do_you;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getDo_you_smoke() {
            return do_you_smoke;
        }

        public void setDo_you_smoke(String do_you_smoke) {
            this.do_you_smoke = do_you_smoke;
        }

        public int getLooking_for() {
            return looking_for;
        }

        public void setLooking_for(int looking_for) {
            this.looking_for = looking_for;
        }

        public String getSplit_bill() {
            return split_bill;
        }

        public void setSplit_bill(String split_bill) {
            this.split_bill = split_bill;
        }

        public String getHide_adverts() {
            return hide_adverts;
        }

        public void setHide_adverts(String hide_adverts) {
            this.hide_adverts = hide_adverts;
        }

        public String getDo_you_want_kids() {
            return do_you_want_kids;
        }

        public void setDo_you_want_kids(String do_you_want_kids) {
            this.do_you_want_kids = do_you_want_kids;
        }

        public String getAbout_yourself() {
            return about_yourself;
        }

        public void setAbout_yourself(String about_yourself) {
            this.about_yourself = about_yourself;
        }

        public String getAre_you_a() {
            return are_you_a;
        }

        public void setAre_you_a(String are_you_a) {
            this.are_you_a = are_you_a;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAny_allergies() {
            return any_allergies;
        }

        public void setAny_allergies(String any_allergies) {
            this.any_allergies = any_allergies;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAny_allergies_status() {
            return any_allergies_status;
        }

        public void setAny_allergies_status(String any_allergies_status) {
            this.any_allergies_status = any_allergies_status;
        }

        public String getPersonality() {
            return personality;
        }

        public void setPersonality(String personality) {
            this.personality = personality;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDistance_invisible() {
            return distance_invisible;
        }

        public void setDistance_invisible(String distance_invisible) {
            this.distance_invisible = distance_invisible;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getDont_show_my_age() {
            return dont_show_my_age;
        }

        public void setDont_show_my_age(String dont_show_my_age) {
            this.dont_show_my_age = dont_show_my_age;
        }


        public List<String> getImage() {
            return image;
        }

        public void setImage(List<String> image) {
            this.image = image;
        }

        public List<String> getVideo() {
            return video;
        }

        public void setVideo(List<String> video) {
            this.video = video;
        }

        public String getFood_preference() {
            return food_preference;
        }

        public void setFood_preference(String food_preference) {
            this.food_preference = food_preference;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getUnlimited_likes() {
            return unlimited_likes;
        }

        public void setUnlimited_likes(String unlimited_likes) {
            this.unlimited_likes = unlimited_likes;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getFoodatext_boost() {
            return foodatext_boost;
        }

        public void setFoodatext_boost(String foodatext_boost) {
            this.foodatext_boost = foodatext_boost;
        }

        public String getMarital_status() {
            return marital_status;
        }

        public void setMarital_status(String marital_status) {
            this.marital_status = marital_status;
        }

        public String getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(String profile_image) {
            this.profile_image = profile_image;
        }

        public int getInterest_in() {
            return interest_in;
        }

        public void setInterest_in(int interest_in) {
            this.interest_in = interest_in;
        }

        public String getAllowing_you() {
            return allowing_you;
        }

        public void setAllowing_you(String allowing_you) {
            this.allowing_you = allowing_you;
        }

        public String getDo_you_have_kids() {
            return do_you_have_kids;
        }

        public void setDo_you_have_kids(String do_you_have_kids) {
            this.do_you_have_kids = do_you_have_kids;
        }

        public String getWho_see_you() {
            return who_see_you;
        }

        public void setWho_see_you(String who_see_you) {
            this.who_see_you = who_see_you;
        }

        public String getWhat_do_you() {
            return what_do_you;
        }

        public void setWhat_do_you(String what_do_you) {
            this.what_do_you = what_do_you;
        }
    }
}
