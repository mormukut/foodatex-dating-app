package com.mms.web_services.requestBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public class UploadImage {
    @Expose
    @SerializedName("image1")
    private MultipartBody.Part image1;

//    @Multipart
//    @POST(WebServices.UPLOAD_SURVEY)
//    Call<UploadSurveyResponseModel> uploadSurvey(@Part MultipartBody.Part[] surveyImage, @Part MultipartBody.Part propertyImage, @Part("DRA") RequestBody dra);

    public MultipartBody.Part getProfilePic() {
        return image1;
    }

    public void setProfilePic(MultipartBody.Part profilePic) {
        this.image1= profilePic;
    }
}
