package com.mms.web_services.requestBean;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 8/14/2018.
 */

public class UserInfoModel {

    @SerializedName("id")
    public String id;
    @SerializedName("name")
    public String name;
    @SerializedName("picture")
    public Picture picture;
    @SerializedName("birthday")
    public String birthday;
    @SerializedName("gender")
    public String gender;
    @SerializedName("link")
    public String link;
    @SerializedName("locale")
    public String locale;
    @SerializedName("timezone")
    public double timezone;
    @SerializedName("updated_time")
    public String updated_time;
    @SerializedName("verified")
    public boolean verified;
    @SerializedName("email")
    public String email;

    public UserInfoModel(String id, String name, Picture picture, String birthday, String gender, String link, String locale, double timezone, String updated_time, boolean verified, String email) {
        this.id = id;
        this.name = name;
        this.picture = picture;
        this.birthday = birthday;
        this.gender = gender;
        this.link = link;
        this.locale = locale;
        this.timezone = timezone;
        this.updated_time = updated_time;
        this.verified = verified;
        this.email = email;
    }

    public static class Data {
        @SerializedName("is_silhouette")
        public boolean is_silhouette;
        @SerializedName("url")
        public String url;

        public boolean is_silhouette() {
            return is_silhouette;
        }

        public void setIs_silhouette(boolean is_silhouette) {
            this.is_silhouette = is_silhouette;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class Picture {
        @SerializedName("data")
        public Data data;

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public double getTimezone() {
        return timezone;
    }

    public void setTimezone(double timezone) {
        this.timezone = timezone;
    }

    public String getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(String updated_time) {
        this.updated_time = updated_time;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
