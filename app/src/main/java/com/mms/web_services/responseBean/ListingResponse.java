package com.mms.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mormukut singh R@J@W@T on 5/14/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : buzzcutz
 */

public class ListingResponse {


    @Expose
    @SerializedName("responsedata")
    private List<Responsedata> responsedata;
    @Expose
    @SerializedName("responsemessage")
    private String responsemessage;
    @Expose
    @SerializedName("responsecode")
    private int responsecode;

    public List<Responsedata> getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(List<Responsedata> responsedata) {
        this.responsedata = responsedata;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public int getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(int responsecode) {
        this.responsecode = responsecode;
    }

    public static class Responsedata implements Serializable {
        @Expose
        @SerializedName("left")
        private String left;
        @Expose
        @SerializedName("ureaction")
        private int ureaction;
        @Expose
        @SerializedName("unlike")
        private int unlike;
        @Expose
        @SerializedName("like")
        private int like;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("added")
        private String added;
        @Expose
        @SerializedName("content")
        private String content;
        @Expose
        @SerializedName("placeholder")
        private String placeholder;
        @Expose
        @SerializedName("video")
        private String video;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("color")
        private String color;
        @Expose
        @SerializedName("cat_name")
        private String cat_name;
        @Expose
        @SerializedName("catid")
        private String catid;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("userid")
        private String userid;
        @Expose
        @SerializedName("id")
        private String id;

        public String getLeft() {
            return left;
        }

        public void setLeft(String left) {
            this.left = left;
        }

        public int getUreaction() {
            return ureaction;
        }

        public void setUreaction(int ureaction) {
            this.ureaction = ureaction;
        }

        public int getUnlike() {
            return unlike;
        }

        public void setUnlike(int unlike) {
            this.unlike = unlike;
        }

        public int getLike() {
            return like;
        }

        public void setLike(int like) {
            this.like = like;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAdded() {
            return added;
        }

        public void setAdded(String added) {
            this.added = added;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getPlaceholder() {
            return placeholder;
        }

        public void setPlaceholder(String placeholder) {
            this.placeholder = placeholder;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public String getCatid() {
            return catid;
        }

        public void setCatid(String catid) {
            this.catid = catid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
