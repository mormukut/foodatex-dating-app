package com.mms.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mormukut singh R@J@W@T on 5/12/2018
 * Company Name : DwebGyus
 * UserName : Mormukutsinghji@gmail.com
 * URL : http:??dwebguys.com/
 * Project Name : buzzcutz
 */

public class LoginResponse {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("profile_staus")
    private String profile_staus;
    @Expose
    @SerializedName("status")
    private String status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProfile_staus() {
        return profile_staus;
    }

    public void setProfile_staus(String profile_staus) {
        this.profile_staus = profile_staus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("profile_image")
        private String profile_image;
        @Expose
        @SerializedName("split_bill")
        private String split_bill;
        @Expose
        @SerializedName("about_yourself")
        private String about_yourself;
        @Expose
        @SerializedName("video")
        private String video;
        @Expose
        @SerializedName("do_you_smoke")
        private String do_you_smoke;
        @Expose
        @SerializedName("do_you_have_kids")
        private String do_you_have_kids;
        @Expose
        @SerializedName("marital_status")
        private String marital_status;
        @Expose
        @SerializedName("do_you_want_kids")
        private String do_you_want_kids;
        @Expose
        @SerializedName("what_do_you")
        private String what_do_you;
        @Expose
        @SerializedName("personality")
        private String personality;
        @Expose
        @SerializedName("height")
        private String height;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("any_allergies")
        private String any_allergies;
        @Expose
        @SerializedName("any_allergies_status")
        private String any_allergies_status;
        @Expose
        @SerializedName("are_you_a")
        private String are_you_a;
        @Expose
        @SerializedName("looking_for")
        private String looking_for;
        @Expose
        @SerializedName("interest_in")
        private String interest_in;
        @Expose
        @SerializedName("food_preference")
        private String food_preference;
        @Expose
        @SerializedName("longitude")
        private String longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("dob")
        private String dob;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("phone")
        private String phone;
        @Expose
        @SerializedName("country_code")
        private String country_code;
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("fullname")
        private String fullname;
        @Expose
        @SerializedName("username")
        private String username;
        @Expose
        @SerializedName("user_id")
        private String user_id;

        public String getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(String profile_image) {
            this.profile_image = profile_image;
        }

        public String getSplit_bill() {
            return split_bill;
        }

        public void setSplit_bill(String split_bill) {
            this.split_bill = split_bill;
        }

        public String getAbout_yourself() {
            return about_yourself;
        }

        public void setAbout_yourself(String about_yourself) {
            this.about_yourself = about_yourself;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getDo_you_smoke() {
            return do_you_smoke;
        }

        public void setDo_you_smoke(String do_you_smoke) {
            this.do_you_smoke = do_you_smoke;
        }

        public String getDo_you_have_kids() {
            return do_you_have_kids;
        }

        public void setDo_you_have_kids(String do_you_have_kids) {
            this.do_you_have_kids = do_you_have_kids;
        }

        public String getMarital_status() {
            return marital_status;
        }

        public void setMarital_status(String marital_status) {
            this.marital_status = marital_status;
        }

        public String getDo_you_want_kids() {
            return do_you_want_kids;
        }

        public void setDo_you_want_kids(String do_you_want_kids) {
            this.do_you_want_kids = do_you_want_kids;
        }

        public String getWhat_do_you() {
            return what_do_you;
        }

        public void setWhat_do_you(String what_do_you) {
            this.what_do_you = what_do_you;
        }

        public String getPersonality() {
            return personality;
        }

        public void setPersonality(String personality) {
            this.personality = personality;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAny_allergies() {
            return any_allergies;
        }

        public void setAny_allergies(String any_allergies) {
            this.any_allergies = any_allergies;
        }

        public String getAny_allergies_status() {
            return any_allergies_status;
        }

        public void setAny_allergies_status(String any_allergies_status) {
            this.any_allergies_status = any_allergies_status;
        }

        public String getAre_you_a() {
            return are_you_a;
        }

        public void setAre_you_a(String are_you_a) {
            this.are_you_a = are_you_a;
        }

        public String getLooking_for() {
            return looking_for;
        }

        public void setLooking_for(String looking_for) {
            this.looking_for = looking_for;
        }

        public String getInterest_in() {
            return interest_in;
        }

        public void setInterest_in(String interest_in) {
            this.interest_in = interest_in;
        }

        public String getFood_preference() {
            return food_preference;
        }

        public void setFood_preference(String food_preference) {
            this.food_preference = food_preference;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }
    }
}
