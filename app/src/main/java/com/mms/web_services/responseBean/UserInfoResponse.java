package com.mms.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 9/5/2018.
 */

public class UserInfoResponse {


    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("status")
    private String status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("images")
        private List<Images> images;
        @Expose
        @SerializedName("profile_image")
        private String profile_image;
        @Expose
        @SerializedName("who_see_you")
        private String who_see_you;
        @Expose
        @SerializedName("distance_invisible")
        private String distance_invisible;
        @Expose
        @SerializedName("dont_show_my_age")
        private String dont_show_my_age;
        @Expose
        @SerializedName("hide_adverts")
        private String hide_adverts;
        @Expose
        @SerializedName("allowing_you")
        private String allowing_you;
        @Expose
        @SerializedName("foodatext_boost")
        private String foodatext_boost;
        @Expose
        @SerializedName("unlimited_likes")
        private String unlimited_likes;
        @Expose
        @SerializedName("split_bill")
        private String split_bill;
        @Expose
        @SerializedName("about_yourself")
        private String about_yourself;
        @Expose
        @SerializedName("video")
        private String video;
        @Expose
        @SerializedName("do_you_smoke")
        private String do_you_smoke;
        @Expose
        @SerializedName("do_you_have_kids")
        private String do_you_have_kids;
        @Expose
        @SerializedName("marital_status")
        private String marital_status;
        @Expose
        @SerializedName("do_you_want_kids")
        private String do_you_want_kids;
        @Expose
        @SerializedName("what_do_you")
        private String what_do_you;
        @Expose
        @SerializedName("personality")
        private String personality;
        @Expose
        @SerializedName("height")
        private String height;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("any_allergies")
        private String any_allergies;
        @Expose
        @SerializedName("any_allergies_status")
        private String any_allergies_status;
        @Expose
        @SerializedName("are_you_a")
        private String are_you_a;
        @Expose
        @SerializedName("looking_for")
        private String looking_for;
        @Expose
        @SerializedName("interest_in")
        private String interest_in;
        @Expose
        @SerializedName("food_preference")
        private String food_preference;
        @Expose
        @SerializedName("longitude")
        private String longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("dob")
        private String dob;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("phone")
        private String phone;
        @Expose
        @SerializedName("country_code")
        private String country_code;
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("fullname")
        private String fullname;
        @Expose
        @SerializedName("username")
        private String username;
        @Expose
        @SerializedName("user_id")
        private String user_id;

        public List<Images> getImages() {
            return images;
        }

        public void setImages(List<Images> images) {
            this.images = images;
        }

        public String getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(String profile_image) {
            this.profile_image = profile_image;
        }

        public String getWho_see_you() {
            return who_see_you;
        }

        public void setWho_see_you(String who_see_you) {
            this.who_see_you = who_see_you;
        }

        public String getDistance_invisible() {
            return distance_invisible;
        }

        public void setDistance_invisible(String distance_invisible) {
            this.distance_invisible = distance_invisible;
        }

        public String getDont_show_my_age() {
            return dont_show_my_age;
        }

        public void setDont_show_my_age(String dont_show_my_age) {
            this.dont_show_my_age = dont_show_my_age;
        }

        public String getHide_adverts() {
            return hide_adverts;
        }

        public void setHide_adverts(String hide_adverts) {
            this.hide_adverts = hide_adverts;
        }

        public String getAllowing_you() {
            return allowing_you;
        }

        public void setAllowing_you(String allowing_you) {
            this.allowing_you = allowing_you;
        }

        public String getFoodatext_boost() {
            return foodatext_boost;
        }

        public void setFoodatext_boost(String foodatext_boost) {
            this.foodatext_boost = foodatext_boost;
        }

        public String getUnlimited_likes() {
            return unlimited_likes;
        }

        public void setUnlimited_likes(String unlimited_likes) {
            this.unlimited_likes = unlimited_likes;
        }

        public String getSplit_bill() {
            return split_bill;
        }

        public void setSplit_bill(String split_bill) {
            this.split_bill = split_bill;
        }

        public String getAbout_yourself() {
            return about_yourself;
        }

        public void setAbout_yourself(String about_yourself) {
            this.about_yourself = about_yourself;
        }

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getDo_you_smoke() {
            return do_you_smoke;
        }

        public void setDo_you_smoke(String do_you_smoke) {
            this.do_you_smoke = do_you_smoke;
        }

        public String getDo_you_have_kids() {
            return do_you_have_kids;
        }

        public void setDo_you_have_kids(String do_you_have_kids) {
            this.do_you_have_kids = do_you_have_kids;
        }

        public String getMarital_status() {
            return marital_status;
        }

        public void setMarital_status(String marital_status) {
            this.marital_status = marital_status;
        }

        public String getDo_you_want_kids() {
            return do_you_want_kids;
        }

        public void setDo_you_want_kids(String do_you_want_kids) {
            this.do_you_want_kids = do_you_want_kids;
        }

        public String getWhat_do_you() {
            return what_do_you;
        }

        public void setWhat_do_you(String what_do_you) {
            this.what_do_you = what_do_you;
        }

        public String getPersonality() {
            return personality;
        }

        public void setPersonality(String personality) {
            this.personality = personality;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAny_allergies() {
            return any_allergies;
        }

        public void setAny_allergies(String any_allergies) {
            this.any_allergies = any_allergies;
        }

        public String getAny_allergies_status() {
            return any_allergies_status;
        }

        public void setAny_allergies_status(String any_allergies_status) {
            this.any_allergies_status = any_allergies_status;
        }

        public String getAre_you_a() {
            return are_you_a;
        }

        public void setAre_you_a(String are_you_a) {
            this.are_you_a = are_you_a;
        }

        public String getLooking_for() {
            return looking_for;
        }

        public void setLooking_for(String looking_for) {
            this.looking_for = looking_for;
        }

        public String getInterest_in() {
            return interest_in;
        }

        public void setInterest_in(String interest_in) {
            this.interest_in = interest_in;
        }

        public String getFood_preference() {
            return food_preference;
        }

        public void setFood_preference(String food_preference) {
            this.food_preference = food_preference;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }
    }

    public static class Images {
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("user_images_id")
        private String user_images_id;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getUser_images_id() {
            return user_images_id;
        }

        public void setUser_images_id(String user_images_id) {
            this.user_images_id = user_images_id;
        }
    }
}
