package com.mms.web_services.responseBean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 9/3/2018.
 */

public class UserListNearByResponse {


    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("status")
    private String status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("user_list")
        private List<User_list> user_list;

        public List<User_list> getUser_list() {
            return user_list;
        }

        public void setUser_list(List<User_list> user_list) {
            this.user_list = user_list;
        }
    }

    public static class User_list {
        @Expose
        @SerializedName("video")
        private String video;
        @Expose
        @SerializedName("profile_image")
        private String profile_image;
        @Expose
        @SerializedName("distance")
        private String distance;
        @Expose
        @SerializedName("dob")
        private String dob;
        @Expose
        @SerializedName("fullname")
        private String fullname;
        @Expose
        @SerializedName("user_id")
        private String user_id;

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(String profile_image) {
            this.profile_image = profile_image;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }
    }
}
